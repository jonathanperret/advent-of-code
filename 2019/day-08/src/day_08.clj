(ns day-08
  (:require
   [clojure.java.io :as io]
   [clojure.string :as string]))

(defn parse-pixels [image-data]
  (map #(Integer/parseInt (str %)) image-data))

(defn split-into-layers [image-data width height]
  (partition (* width height) image-data))

(defn count-digit [layer digit]
  (->> layer (filter #(= % digit)) count))

(defn find-fewest-zeros [layers]
  (apply min-key #(count-digit % 0) layers))

(defn part-1 [pixels width height]
  (let [layers (split-into-layers pixels width height)
        layer (find-fewest-zeros layers)]
    (* (count-digit layer 1) (count-digit layer 2))))

(defn nilify-transparent [digits]
  (map #(when-not (= % 2) %) digits))

(defn stack-layers [layers]
  (apply map (comp #(some identity %) vector) layers))

(defn format-layer [layer width]
  (->> layer (map {0 " ", 1 "█"}) (partition width) (map #(str (apply str %) "\n")) (apply str)))

(defn part-2 [pixels width height]
  (-> pixels nilify-transparent (split-into-layers width height) stack-layers (format-layer width)))

(defn -main []
  (with-open [f (io/reader "input.txt")]
    (let [pixels (parse-pixels (first (line-seq f)))
          width 25, height 6]
      (println (part-1 pixels width height))
      (println (part-2 pixels width height)))))
