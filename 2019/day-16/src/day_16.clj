(ns day-16
  (:require
   [clojure.java.io :as io]
   [clojure.string :as string]))

;;; Part 1
(defn pattern [i]
  (next (mapcat #(repeat (inc i) %)
                (apply concat (repeat [0 1 0 -1])))))

(defn ones-digit [n]
  (mod (Math/abs n) 10))

(defn convolute [a b]
  (apply + (map * a b)))

(defn phase [input]
  (map
   #(ones-digit (convolute input %))
   (map pattern (range (count input)))))

(defn part-1 [input]
  (string/join (take 8 (first (drop 100 (iterate phase input))))))

;;; Parse the input
(defn parse [input]
  (map #(Integer/parseInt (str %)) input))

;;; Part 2
(defn reverse-tail [input repetitions offset]
  (let [total-length (* repetitions (count input))
        tail-length (- total-length offset)]
    (take tail-length (mapcat identity (repeat (reverse input))))))

(defn offset [input]
  (reduce #(+ (* %1 10) %2) 0 (take 7 input)))

(defn step [input]
  (reductions #(mod (+ %1 %2) 10) input))

(defn part-2 [input]
  (string/join (reverse (take-last 8 (first (drop 100 (iterate step (reverse-tail input 10000 (offset input)))))))))

(defn -main []
  (with-open [f (io/reader "input.txt")]
    (let [input (parse (first (line-seq f)))]
      (println (part-1 input))
      (println (part-2 input)))))
