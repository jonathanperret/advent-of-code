(ns day-07
  (:require
   [clojure.core.async :as async :refer [chan <!! >!! close!]]
   [clojure.math.combinatorics :as combo]
   [intcode :refer [parse-file IO in out run]]))

(defn pipe-io [phases]
  (let [next-phases (volatile! phases)]
    (reify IO
      (in [_]
        (let [[v] @next-phases]
          (vswap! next-phases rest)
          v))
      (out [_ v]
        (vswap! next-phases #(let [[h & t] %]
                               (if h
                                 (conj t v h)
                                 (conj t v))))))))

(defn thruster-signal [program phases]
  (let [p (pipe-io phases)]
    (out p 0)
    (dotimes [n (count phases)]
      (run program p))
    (in p)))

(defn max-thruster-signal [program]
  (apply max (map #(thruster-signal program %) (combo/permutations (range 5)))))

(defn iterative-thruster-signal [program phases]
  (let [result (chan)
        amp-count (count phases)
        amp-outputs (vec (repeatedly amp-count #(chan 1)))
        amp-inputs (mapv #(amp-outputs (mod (dec %) amp-count)) (range amp-count))]
    (doseq [amp (range amp-count)]
      (future
        (run program (reify IO
                       (in [_]
                         (<!! (amp-inputs amp)))
                       (out [_ v]
                         (>!! (amp-outputs amp) v))))
        (when (= amp (dec amp-count))
          (>!! result (<!! (amp-outputs amp)))))
      (>!! (amp-inputs amp) (phases amp)))
    (>!! (amp-inputs 0) 0)
    (let [v (<!! result)]
      (doseq [c amp-outputs]
        (close! c))
      v)))

(defn max-iterative-thruster-signal [program]
  (apply max (map #(iterative-thruster-signal program %) (combo/permutations (range 5 10)))))

(defn -main
  []
  (let [instructions (parse-file "input.txt")]
    (println (max-thruster-signal instructions))
    (println (max-iterative-thruster-signal instructions))
    (shutdown-agents)))
