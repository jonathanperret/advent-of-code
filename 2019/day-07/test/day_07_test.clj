(ns day-07-test
  (:require
   [day-07 :refer :all]
   [intcode :refer [in out]]
   [clojure.test :refer [deftest is testing]]))

(deftest pipe-io-test
  (let [p (pipe-io [10 20 30])]
    (out p 0) ; Input to pipe
    (is (= 10 (in p)) "Read first phase")
    (is (= 0 (in p)) "Read input to pipe")
    (out p 1) ; Send output to next stage
    (is (= 20 (in p)) "Read second phase")
    (is (= 1 (in p)) "Read output from previous stage")
    (out p 2) ; Send output to next stage
    (is (= 30 (in p)) "Read third phase")
    (is (= 2 (in p)))
    (out p 3)
    (is (= 3 (in p)))))

(deftest thruster-signal-test
  (is (= 43210 (thruster-signal [3 15 3 16 1002 16 10 16 1 16 15 15 4 15 99 0 0]
                                [4 3 2 1 0])))
  (is (= 54321 (thruster-signal [3 23 3 24 1002 24 10 24 1002 23 -1 23 101 5 23 23 1 24 23 23 4 23 99 0 0]
                                [0 1 2 3 4])))
  (is (= 65210 (thruster-signal [3 31 3 32 1002 32 10 32 1001 31 -2 31 1007 31 0 33 1002 33 7 33 1 33 31 31 1 32 31 31 4 31 99 0 0 0]
                                [1 0 4 3 2]))))

(deftest max-thruster-signal-test
  (is (= 43210 (max-thruster-signal [3 15 3 16 1002 16 10 16 1 16 15 15 4 15 99 0 0])))
  (is (= 54321 (max-thruster-signal [3 23 3 24 1002 24 10 24 1002 23 -1 23 101 5 23 23 1 24 23 23 4 23 99 0 0])))
  (is (= 65210 (max-thruster-signal [3 31 3 32 1002 32 10 32 1001 31 -2 31 1007 31 0 33 1002 33 7 33 1 33 31 31 1 32 31 31 4 31 99 0 0 0]))))

(deftest iterative-thruster-signal-test
  (is (= 139629729
         (iterative-thruster-signal [3 26 1001 26 -4 26 3 27 1002 27 2 27 1 27 26 27 4 27 1001 28 -1 28 1005 28 6 99 0 0 5]
                                    [9 8 7 6 5]))))

(deftest max-iterative-thruster-signal-test
  (is (= 139629729
         (max-iterative-thruster-signal [3 26 1001 26 -4 26 3 27 1002 27 2 27 1 27 26 27 4 27 1001 28 -1 28 1005 28 6 99 0 0 5]))))
