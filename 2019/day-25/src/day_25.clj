(ns day-25
  (:require
   [clojure.string :as string]
   [intcode :refer [parse-file IO run]]))

(defn io [reader writer]
  (reify IO
    (in [_]
      (.read reader))
    (out [_ val]
      (.write writer val)
      (.flush writer))))

(defn -main []
  (let [instructions (parse-file "input.txt")]
    (run instructions (io *in* *out*))))
