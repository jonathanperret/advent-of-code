S = start
D = dark matter
M = manifold
J = jam
C = candy cane
m = molten lava
p = photons
A = antenna
g = giant electromagnet
e = escape pod

Do not take
- jam
- bowl of rice
- manifold
- dark matter

take
- antenna
- candy cane
- hypercube
- dehydrated water


     m
     |
 J-M-C
   |
   .-p
   | |
   D-A
   | |
 e-S g
 |
-.

> hypercube
> hypercube + jam
> hypercube + candy cane + jam
> hypercube + dehydrated water 
> hypercube + candy cane + dehydrated water 
< hypercube + jam + dehydrated water 
< hypercube + bowl of rice
