(ns day-25-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [day-25 :refer :all]
   [intcode :refer [in out step]]))

(deftest test-hop
  (let [[state io] (hop NORTH)]
    (is (= NORTH (in io))
        "Feed the move to the program")
    (out io OPEN)
    (is (= OPEN @state)
        "Record the outcome of the program")))

(defn fake-steps [& steps]
  (let [remaining (volatile! steps)]
    (fn [state]
      (let [[step] @remaining]
        (vswap! remaining rest)
        (step state)))))

(deftest test-exec
  (testing "run until a result is found"
    (with-redefs [step (fake-steps
                        (fn [state] (is (= EAST (in (:io state)))) state)
                        identity
                        identity
                        identity
                        (fn [state] (out (:io state) OXYGEN) state)
                        (fn [_] (throw (Exception. "Should never get called"))))]
      (is (= OXYGEN (:result (exec nil EAST))))))
  (testing "stop if the program stops (should not happen?)"
    (with-redefs [step (fake-steps
                        (fn [state] (assoc state :done true)))]
      (is (nil? (:result (exec nil nil))))))
  (testing "return updated program state"
    (with-redefs [step (fake-steps
                        (fn [state] (out (:io state) OXYGEN) (assoc state :updated true)))]
      (is (:updated (:program-state (exec nil EAST)))))))

(deftest test-add-vector
  (is (= [3 5]
         (add-vector [1 0] [1 2] [1 3]))))

(deftest test-next-destinations
  (is (= [{:move NORTH :distance 3 :position [-1, -2]}
          {:move SOUTH :distance 3 :position [-1, 0]}
          {:move WEST :distance 3 :position [-2, -1]}
          {:move EAST :distance 3 :position [0, -1]}]
         (next-destinations {:distance 2, :position [-1, -1]}))))

(deftest test-explore-destination
  (with-redefs [step (fake-steps
                      (fn [state] (is (= NORTH (in (:io state)))) state)
                      (fn [state] (out (:io state) WALL) state))]
    (is (= {[0 0] OPEN, [0 -1] WALL}
           (:seen (explore-destination {:seen {[0 0] OPEN}, :destinations [], :continue #{OPEN}}
                                       {:move NORTH, :position [0 -1]})))
        "add explored tile to seen list"))

  (with-redefs [step (fake-steps
                      (fn [state] (is (= NORTH (in (:io state)))) state)
                      (fn [state] (out (:io state) OPEN) state))]
    (is (= [{:move SOUTH, :distance 1, :position [0 1]}
            {:move NORTH, :distance 2, :position [0 -2]}
            {:move SOUTH, :distance 2, :position [0 0]}
            {:move WEST, :distance 2, :position [-1 -1]}
            {:move EAST, :distance 2, :position [1 -1]}]
           (map #(select-keys % [:move :distance :position])
                (:destinations (explore-destination {:seen {[0 0] OPEN}, :destinations [{:move SOUTH, :distance 1, :position [0 1]}], :continue #{OPEN}}
                                                    {:move NORTH, :distance 1, :position [0 -1]}))))
        "enqueue possible moves (even if already visited: this is pruned by `explore`)"))

  (with-redefs [step (fake-steps
                      (fn [state] (is (= NORTH (in (:io state)))) state)
                      (fn [state] (out (:io state) WALL) state))]
    (is (= [{:move SOUTH, :position [0 1]}]
           (:destinations (explore-destination {:seen {[0 0] OPEN}
                                                :destinations [{:move SOUTH, :position [0 1]}]
                                                :continue #{OPEN}}
                                               {:move NORTH, :position [0 -1]})))
        "No new possible moves starting from a wall"))

  (with-redefs [step (fake-steps
                      (fn [state] (is (= NORTH (in (:io state)))) state)
                      (fn [state] (out (:io state) OXYGEN) state))]
    (is (= [0 -1] (:position (:oxygen (explore-destination {:seen {} :destinations [], :continue #{OPEN}}
                                                           {:move NORTH, :position [0 -1]}))))
        "Report position of oxygen"))

  (with-redefs [step (fake-steps
                      (fn [state] (is (= EAST (in (:io state)))) state)
                      (fn [state] (out (:io state) OPEN) state))]
    (is (= 2 (:distance (explore-destination {:seen {} :destinations [], :continue #{OPEN}}
                                             {:move EAST, :distance 2, :position [1 -1]})))
        "Report distance to furthest explored point")))

(deftest test-explore
  (is (= {:seen {[0 0] OPEN}, :destinations []}
         (select-keys
          (explore
           {:seen {[0 0] OPEN}
            :destinations (queue {:move SOUTH, :position [0 0], :distance 1})
            :continue #{OPEN}})
          [:seen :destinations]))
      "Ignore destinations that have already been visited")
  (with-redefs [step (fake-steps
                      (fn [state] (is (= SOUTH (in (:io state)))) state)
                      (fn [state] (out (:io state) OPEN) state))]
    (testing "Visit unknown destination"
      (let [{:keys [seen destinations]}
            (explore
             {:seen {[0 0] OPEN}
              :destinations (queue {:move SOUTH, :distance 1, :position [0 1]})
              :continue #{OPEN}})]
        (is (= {[0 0] OPEN, [0 1] OPEN} seen))
        (is (= [{:move NORTH, :distance 2, :position [0 0]}
                {:move SOUTH, :distance 2, :position [0 2]}
                {:move WEST,  :distance 2, :position [-1 1]}
                {:move EAST,  :distance 2, :position [1 1]}]
               (map #(select-keys % [:move :distance :position]) destinations)))))))

(defn request-direction [expected-program-state expected-direction]
  (fn [state]
    (is (= expected-program-state (:memory state)))
    (is (= expected-direction (in (:io state))))
    state))

(defn report-tile [program-state tile]
  (fn [state]
    (out (:io state) tile)
    (assoc state :memory program-state)))

(deftest test-dist-to-oxygen
  (with-redefs [step (fake-steps
                      (request-direction "INIT" NORTH)
                      (report-tile "1 NORTH" OPEN)

                      (request-direction "INIT" SOUTH)
                      (report-tile "1 SOUTH" WALL)

                      (request-direction "INIT" WEST)
                      (report-tile "1 WEST" OPEN)

                      (request-direction "INIT" EAST)
                      (report-tile "1 EAST" OPEN)

                      (request-direction "1 NORTH" NORTH)
                      (report-tile "2 NORTH" OPEN)

                      (request-direction "1 NORTH" WEST)
                      (report-tile "1 NORTH 1 WEST" OXYGEN))]
    (is (= {:distance 2 :position [-1 -1]}
           (select-keys (find-oxygen "INIT") [:distance :position])))))
