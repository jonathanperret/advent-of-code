(ns day-11-test
  (:require
   [day-11 :refer :all]
   [clojure.test :refer [deftest is testing]]))

(deftest test-turn
  (is (= [1 0] (:direction (turn {:direction [0 -1]} 1))))
  (is (= [-1 0] (:direction (turn {:direction [0 -1]} 0)))))

(deftest test-bounding-box
  (is (= [[-3 0] [5 2]]
         (bounding-box {[-1 1] nil
                        [5 0] nil
                        [-3 2] nil}))))
