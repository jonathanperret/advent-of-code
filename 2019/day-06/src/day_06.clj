(ns day-06
  (:require
   [clojure.java.io :as io]
   [clojure.set :as set]
   [clojure.string :as string]))

(defn parse-line [l]
  (let [[center satellite] (string/split l #"\)")]
    {:center center :satellite satellite}))

(defn center-of-mass [orbits]
  (let [centers (set (map :center orbits))
        satellites (set (map :satellite orbits))]
    (first (set/difference centers satellites))))

(defn topological-sort [orbits]
  (let [by-center (group-by :center orbits)]
    (tree-seq (constantly true)
              #(by-center (:satellite %))
              {:satellite (center-of-mass orbits)})))

(defn reduce-ancestors [f init sorted-orbits]
  (reduce (fn [ancestors {:keys [satellite center]}]
            (assoc ancestors satellite (if center
                                         (f (ancestors center) center)
                                         init)))
          {}
          sorted-orbits))

(defn orbit-counts [sorted-orbits]
  (reduce-ancestors (fn [count _] (inc count)) 0 sorted-orbits))

(defn deep-orbits [sorted-orbits]
  (reduce-ancestors (fn [orbit center] (conj orbit center)) [] sorted-orbits))

(defn checksum [sorted-orbits]
    (apply + (vals (orbit-counts sorted-orbits))))

(defn common-prefix-count [l1 l2]
  (count (take-while #(apply = %) (map vector l1 l2))))

(defn dist-to-santa [sorted-orbits]
  (let [{san "SAN", you "YOU"} (deep-orbits sorted-orbits)
        common-count (common-prefix-count san you)]
    (- (+ (count san) (count you))
       (* 2 common-count))))

(defn -main []
  (with-open [f (io/reader "input.txt")]
    (let [orbits (topological-sort (map parse-line (line-seq f)))]
      (println (checksum orbits))
      (println (dist-to-santa orbits)))))
