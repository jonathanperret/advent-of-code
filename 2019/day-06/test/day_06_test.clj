(ns day-06-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [clojure.test.check :as tc]
   [clojure.test.check.clojure-test :refer [defspec]]
   [clojure.test.check.generators :as gen]
   [clojure.test.check.properties :as prop]
   [clojure.zip :as zip]
   [day-06 :refer :all]))

(deftest test-parse-line
  (is (= {:center "a", :satellite "b"} (parse-line "a)b"))))

(deftest test-center-of-mass
  (is (= "a" (center-of-mass [(parse-line "b)c")
                              (parse-line "a)b")
                              (parse-line "b)d")]))))

(def gen-leaf
  (gen/return {}))

(defn gen-branch [inner]
  (gen/let [children (gen/not-empty (gen/vector inner))]
    {:children children}))

(defn gen-node-ids [count]
  (gen/vector-distinct (gen/not-empty gen/string-alphanumeric) {:num-elements count}))

(def gen-orbits
  (gen/let [tree (gen-branch (gen/recursive-gen gen-branch gen-leaf))]
    (let [z (zip/zipper map? :children (fn [n c] (assoc n :children c)) tree)
          node-count (count (take-while (complement zip/end?) (iterate zip/next z)))]
      (gen/let [node-ids (gen-node-ids node-count)]
        (gen/shuffle
         (loop [z z
                [id & ids] node-ids
                nodes []]
           (if (zip/end? z)
             nodes
             (let [center (some-> z zip/up zip/node :id)]
               (recur (zip/next (zip/edit z assoc :id id))
                      ids
                      (cond-> nodes center (conj {:satellite id, :center center})))))))))))

(def gen-orbits-and-element
  (gen/let [orbits gen-orbits
            element (gen/elements orbits)]
    [orbits element]))

(defn indices [pred coll]
  (keep-indexed #(when (pred %2) %1) coll))

(defspec test-topological-sort 100
  (prop/for-all [[orbits {:keys [satellite center]}] gen-orbits-and-element]
    (let [sorted (topological-sort orbits)
          satellite-index (first (indices #(= (:satellite %) satellite) sorted))
          center-index (first (indices #(= (:satellite %) center) sorted))]
      (if center
        (< center-index satellite-index)
        (and (nil? center-index)
             (zero? satellite-index))))))

(def orbits (map parse-line ["COM)B"
                             "B)C"
                             "C)D"
                             "D)E"
                             "E)F"
                             "B)G"
                             "G)H"
                             "D)I"
                             "E)J"
                             "J)K"
                             "K)L"]))

(deftest test-orbit-counts
  (let [counts (orbit-counts (topological-sort orbits))]
    (is (= 3 (counts "D")))
    (is (= 7 (counts "L")))
    (is (= 0 (counts "COM")))))

(deftest test-checksum
  (is (= 42 (checksum (topological-sort orbits)))))

(def orbits-with-people (into orbits (map parse-line) ["K)YOU" "I)SAN"]))

(deftest test-deep-orbits
  (let [deep (deep-orbits (topological-sort orbits-with-people))]
    (is (= ["COM" "B" "C" "D" "E" "J" "K"] (deep "YOU")))
    (is (= ["COM" "B" "C" "D" "I"] (deep "SAN")))))

(deftest test-common-prefix-count
  (is (= 4 (common-prefix-count ["COM" "B" "C" "D" "E" "J" "K"]
                                ["COM" "B" "C" "D" "I"]))))

(deftest test-dist-to-santa
  (is (= 4 (dist-to-santa (topological-sort orbits-with-people)))))
