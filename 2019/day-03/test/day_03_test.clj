(ns day-03-test
  (:require
   [day-03 :refer :all]
   [clojure.test :refer [deftest is testing]]))

(deftest test-parse-segment
  (is (= {:dir [ 0  1] :count 15} (parse-segment "U15")))
  (is (= {:dir [ 1  0] :count  2} (parse-segment  "R2")))
  (is (= {:dir [ 0 -1] :count  3} (parse-segment  "D3")))
  (is (= {:dir [-1  0] :count  7} (parse-segment  "L7"))))

(deftest test-parse-wire
  (with-redefs [parse-segment #(str "<" % ">")]
    (is (= ["<U15>", "<R2>", "<L7>"] (parse-wire "U15,R2,L7"))
        "should return the seq of parsed segments")))

(deftest test-trace-segment
  (is (= [{:pos [3 0], :dist 10} {:pos [2 0], :dist 9} {:pos [1 0], :dist 8}] (trace-segment {:pos [0 0], :dist 7} (parse-segment "R3")))
      "should return the points of the segments, in reverse order, excluding the starting point"))

(deftest test-add-segment
  (is (= {:cursor {:pos [0 -2], :dist 2}
          :wire {[0 -1] {:pos [0 -1], :dist 1}, [0 -2] {:pos [0 -2], :dist 2}}}
         (add-segment initial-wire (parse-segment "D2")))))

(deftest test-trace-wire
  (is (= {[0 -1] {:pos [0 -1], :dist 1}
          [0 -2] {:pos [0 -2], :dist 2}
          [1 -2] {:pos [1 -2], :dist 3}}
         (trace-wire (parse-wire "D2,R1"))))
  (testing "self intersecting wire"
    (is (= {[1 0] {:pos [1 0], :dist 1}
            [2 0] {:pos [2 0], :dist 2}
            [2 1] {:pos [2 1], :dist 3}
            [1 1] {:pos [1 1], :dist 4}}
           (trace-wire (parse-wire "R2,U1,L1,D1"))))))

(deftest test-norm
  (is (= 5 (norm [2 -3])) "should return the Manhattan norm"))

(deftest test-least-norm
  (is (= 2 (least-norm #{[2 -3] [1 1] [100 -100]}))
      "should be the (Manhattan) norm of the point closest to the origin"))

(deftest test-common-positions
  (is (= [{:pos [1 0], :dist [2 3]}]
         (common-positions {[1 0] {:pos [1 0], :dist 2}, [2 0] {:pos [2 0], :dist 5}}
                           {[1 0] {:pos [1 0], :dist 3}, [1 2] {:pos [1 2], :dist 7}}))))

(deftest test-part-1
  (let [part-1 (fn [w1 w2] (closest-to-origin (common-positions (trace-wire (parse-wire w1))
                                                                (trace-wire (parse-wire w2)))))]
    (is (= 6 (part-1 "R8,U5,L5,D3" "U7,R6,D4,L4")))
    (is (= 159 (part-1 "R75,D30,R83,U83,L12,D49,R71,U7,L72"
                       "U62,R66,U55,R34,D71,R55,D58,R83")))
    (is (= 135 (part-1 "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
                       "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")))))

(deftest test-part-2
  (let [part-2 (fn [w1 w2] (fewest-steps (common-positions (trace-wire (parse-wire w1))
                                                           (trace-wire (parse-wire w2)))))]
    (is (= 30 (part-2 "R8,U5,L5,D3" "U7,R6,D4,L4")))
    (is (= 610 (part-2 "R75,D30,R83,U83,L12,D49,R71,U7,L72"
                       "U62,R66,U55,R34,D71,R55,D58,R83")))
    (is (= 410 (part-2 "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
                       "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")))))
