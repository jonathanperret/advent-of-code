(ns day-03
  (:require
   [clojure.java.io :as io]
   [clojure.string :as string]))

(defn parse-segment [segment]
  {:dir ({\U [0 1], \D [0 -1], \R [1 0], \L [-1 0]} (first segment))
   :count (Integer/parseInt (subs segment 1))})

(defn parse-wire [input]
  (map parse-segment (string/split input #",")))

(defn trace-segment [{[x y] :pos, dist :dist} {[dx dy] :dir, count :count}]
  (for [c (range count 0 -1)]
    {:pos [(+ x (* dx c)) (+ y (* dy c))], :dist (+ dist c)}))

(defn add-segment [{:keys [cursor wire] :as state} seg]
  (let [positions (trace-segment cursor seg)]
    {:cursor (first positions)
     :wire (merge (zipmap (map :pos positions) positions) wire)}))

(def initial-wire {:cursor {:pos [0 0], :dist 0} :wire {}})

(defn trace-wire [segments]
  (:wire (reduce add-segment initial-wire segments)))

(defn norm [coords]
  (apply + (map #(Math/abs %) coords)))

(defn least-norm [coords-list]
  (apply min (map norm coords-list)))

(defn common-positions [w1 w2]
  (map (fn [{:keys [pos dist]}] {:pos pos :dist [dist (-> pos w2 :dist)]})
       (vals (select-keys w1 (keys w2)))))

(defn closest-to-origin [common-positions]
  (least-norm (map :pos common-positions)))

(defn fewest-steps [common-positions]
  (least-norm (map :dist common-positions)))

(defn -main
  []
  (with-open [input (io/reader "input.txt")]
    (let [[w1 w2] (map #(trace-wire (parse-wire %)) (line-seq input))
          ps (common-positions w1 w2)]
      (println (closest-to-origin ps))
      (println (fewest-steps ps)))))
