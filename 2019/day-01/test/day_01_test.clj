(ns day-01-test
  (:require
   [day-01 :refer :all]
   [clojure.string :as string]
   [clojure.test :refer [deftest is testing]])
  (:import
   (java.io BufferedReader StringReader)))

(deftest module-fuel-test
  (testing "For a mass of 12, divide by 3 and round down to get 4, then subtract 2 to get 2."
    (is (= 2 (module-fuel 12))))
  (testing "For a mass of 14, dividing by 3 and rounding down still yields 4, so the fuel required is also 2."
    (is (= 2 (module-fuel 14))))
  (testing "For a mass of 1969, the fuel required is 654."
    (is (= 654 (module-fuel 1969))))
  (testing "For a mass of 100756, the fuel required is 33583."
    (is (= 33583 (module-fuel 100756)))))

(defn lines-reader [& lines]
  (BufferedReader. (StringReader. (str (string/join "\n" lines) "\n"))))

(deftest lines-reader-test
  (is (= "1\n2\n3\n" (slurp (lines-reader "1" "2" "3")))))

(deftest read-input-test
  (is (= [12 3 456] (read-input (lines-reader "12" "3" "456")))))

(deftest iterated-module-fuel-test
  (testing "A module of mass 14 requires 2 fuel. This fuel requires no further fuel (2 divided by 3 and rounded down is 0, which would call for a negative fuel), so the total fuel required is still just 2."
    (is (=  2 (iterated-module-fuel 14))))
  (testing "At first, a module of mass 1969 requires 654 fuel. Then, this fuel requires 216 more fuel (654 / 3 - 2). 216 then requires 70 more fuel, which requires 21 fuel, which requires 5 fuel, which requires no further fuel. So, the total fuel required for a module of mass 1969 is 654 + 216 + 70 + 21 + 5 = 966."
    (is (=  966 (iterated-module-fuel 1969))))
  (testing "The fuel required by a module of mass 100756 and its fuel is: 33583 + 11192 + 3728 + 1240 + 411 + 135 + 43 + 12 + 2 = 50346."
    (is (=  50346 (iterated-module-fuel 100756)))))
