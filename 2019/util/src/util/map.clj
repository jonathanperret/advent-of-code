(ns util.map
  (:refer-clojure :exclude [read])
  (:require
   [clojure.java.io :as io]))

(defn coord-set [l]
  (set (map first l)))

(defn coord-to-char-map [l]
  (into {} l))

(defn parse
  ([lines] (parse lines {}))
  ([lines {:keys [char coords collect] :or {char #{\#}, coords vector, collect coord-set}}]
   (collect
    (mapcat
     (fn [y line]
       (keep-indexed
        (fn [x content]
          (when-let [e (char content)]
            [(coords x y) e]))
        line))
     (range)
     lines))))

(defn read
  ([]
   (read "input.txt" {}))
  ([reader-or-options]
   (if (map? reader-or-options)
     (read "input.txt" reader-or-options)
     (read reader-or-options {})))
  ([reader options]
   (with-open [f (io/reader reader)]
     (parse (line-seq f) options))))

(defn neighbours-4 [[x y]]
  [[(dec x) y]
   [(inc x) y]
   [x (dec y)]
   [x (inc y)]])

(defn dist [a b]
  (apply + (map #(Math/abs (- %2 %1)) a b)))
