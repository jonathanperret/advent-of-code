(ns util.collections)

(defn weak-assoc
  "Assoc value `v` to key `key` unless there already was a value associated with `key` in `coll`"
  [coll key v]
  (update coll key #(or % v)))

(defn weak-assoc-in
  "Assoc value `v` to nested key `keys` unless there already was a value associated with `keys` in `coll`"
  [coll keys v]
  (update-in coll keys #(or % v)))

(defn find-index
  "First index i such that col[i] = val"
  [col val]
  (first (keep-indexed #(when (= val %2) %1) col)))

;;; Iterated function applications
(defn until-stable
  "The shortest subsequence l' of l such that the element following l' is the same as the last element of l'.

  Intended for applications where l is some (iterate f x)"
  [l]
  (when (seq l)
    (cons (first l) (map second (take-while #(apply not= %) (partition 2 1 l))))))

(defn first-stable
  "The first element a of l such that the element following a is also a.

  Intended for applications where l is some (iterate f x)"
  [l]
  (ffirst (drop-while #(apply not= %) (partition 2 1 l))))

(defn first-indices
  "Seq of maps m[i]. Each m[i] maps values v in l to the first index j in [0..i] such that l[j] = v.

  Intended for applications where l is some (iterate f x)"
  [l]
  (map :first-index
       (reductions #(-> %1
                        (weak-assoc-in [:first-index %2] (:i %1))
                        (update :i inc))
                   {:first-index {} :i 0}
                   l)))

(defn find-period
  "[offset, period], where period is the smallest positive number such that there exists an offset
  such that l[offset + period] = l[offset].

  Intended for applications where l is some (iterate f x)"
  [l]
  (let [indices (first-stable (first-indices l))
        start-of-next-period (count indices)
        start-period-element (nth l start-of-next-period)
        offset (indices start-period-element)]
    [offset (- start-of-next-period offset)]))

(defn first-duplicate
  "l[i], where i is the smallest number such that there exists an i0 < i such that l[i0] = l[i].

  Intended for applications where l is some (iterate f x)"
  [l]
  (reduce #(let [v (conj %1 %2)]
             (if (= v %1)
               (reduced %2)
               v))
          #{}
          l))
