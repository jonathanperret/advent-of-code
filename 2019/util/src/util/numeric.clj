(ns util.numeric)

(defn gcd
  "greatest common divider"
  [a b]
  (if (zero? b)
    a
    (recur b (mod a b))))

(defn lcm
  "least common multiple"
  ([a b]
   (/ (* a b) (gcd a b)))
  ([a b & o]
   (reduce lcm (lcm a b) o)))

(defn xgcd
  "Extended Euclidean Algorithm. Returns [gcd(a,b) x y] where ax + by = gcd(a,b)."
  [a b]
  (if (= a 0)
    [b 0 1]
    (let [[g x y] (xgcd (mod b a) a)]
      [g (- y (* (quot b a) x)) x])))
