(ns util.collections-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [util.collections :refer :all]))

(deftest test-until-stable
  (is (= [1 2 3] (until-stable [1 2 3 3 3 3])))
  (is (= [1] (until-stable [1 1 1 1])))
  (is (nil? (until-stable []))))

(deftest test-first-stable
  (is (= 3 (first-stable [1 2 3 3 3 3])))
  (is (= 1 (first-stable [1 1 1 1])))
  (is (nil? (first-stable []))))

(deftest test-weak-assoc
  (is (= {:a 1} (weak-assoc {} :a 1)))
  (is (= {:b 1} (weak-assoc {:b 1} :b 3))))

(deftest test-weak-assoc-in
  (is (= {:a {:b 1}} (weak-assoc-in {:a {}} [:a :b] 1)))
  (is (= {:a {:b 5}} (weak-assoc-in {:a {:b 5}} [:a :b] 1))))

(deftest test-first-inidices
  (is (= {1 0, 2 1, 4 2, 8 3}
         (last (first-indices [1 2 4 8])))))

(deftest test-find-index
  (is (= 3 (find-index [98 3 999 17 490 17 53] 17)))
  (is (nil? (find-index [ 2 3 4] 17))))

(deftest test-find-period
  (let [[offset period] (find-period [-3 -2 -1 0 1 2 3 0 1 2 3])]
    (is (= 3 offset) "The elements -3 -2 -1 are an offset before the periodic part begins")
    (is (= 4 period) "The periodic part is comprised of the elements 0 1 2 3")))

(deftest test-first-duplicate
  (is (= 5 (first-duplicate [5 0 1 2 3 4 5 0 1]))))
