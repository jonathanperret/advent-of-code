(ns day-21
  (:require
   [clojure.string :as string]
   [intcode :refer [parse-file IO run]]))

(defn io [& input-lines]
  (let [input (volatile! (map int (str (string/join "\n" input-lines) "\n")))]
    (reify IO
      (in [_]
        (let [[c] @input]
          (vswap! input next)
          c))
      (out [_ val]
        (cond
          (< val 256) (print (char val))
          :else (println val))))))

(defn -main []
  (let [instructions (parse-file "input.txt")]
    (run  instructions (io "NOT T T"
                           "AND A T"
                           "AND B T"
                           "AND C T"
                           "NOT T J"
                           "AND D J"
                           "WALK"))
    (run  instructions (io "NOT T T"
                           "AND A T"
                           "AND B T"
                           "AND C T"
                           "NOT T J"
                           "AND J T"
                           "AND D J"
                           "OR E T"
                           "OR H T"
                           "AND T J"
                           "RUN"))))
