(ns day-15
  (:require
   [clojure.string :as string]
   [intcode :refer [parse-file IO step]]))

(def NORTH 1)
(def SOUTH 2)
(def WEST 3)
(def EAST 4)
(def ALL_DIRECTIONS (range 1 5))

(def WALL 0)
(def OPEN 1)
(def OXYGEN 2)

(defn hop [move]
  (let [state (volatile! nil)
        io (reify IO
             (in [_] move)
             (out [_ val] (vreset! state val)))]
    [state io]))

(defn exec
  [program-state move]
  (let [[result io] (hop move)]
    (loop [program-state (assoc program-state :io io)]
      (if (or @result (:done program-state))
        {:result @result, :program-state program-state}
        (recur (step program-state))))))

(def directions {NORTH [0 -1], SOUTH [0 1], WEST [-1 0], EAST [1 0]})

(defn add-vector [& vs]
  (apply mapv + vs))

(defn next-destinations [destination]
  (for [direction ALL_DIRECTIONS]
    (-> destination
        (assoc :move direction)
        (update :distance inc)
        (update :position add-vector (directions direction)))))

(defn update-exploration [{:keys [continue] :as exploration-state} {:keys [position distance] :as destination} {:keys [result, program-state]}]
  (let [destination (assoc destination :program-state program-state)]
    (cond-> exploration-state
      :always (update :seen assoc position result)
      (= result OXYGEN) (assoc :oxygen destination)
      (continue result) (->
                         (assoc :distance distance)
                         (update :destinations into (next-destinations destination))))))

(defn explore-destination [exploration-state {:keys [move program-state] :as destination}]
  (update-exploration exploration-state destination (exec program-state move)))

(def empty-queue clojure.lang.PersistentQueue/EMPTY)

(defn queue [& vals] (into empty-queue vals))

(defn explore [{:keys [seen destinations] :as exploration-state}]
  (let [{:keys [position] :as destination} (peek destinations)
        remaining (pop destinations)]
    (cond-> (assoc exploration-state :destinations remaining)
      (not (seen position)) (explore-destination destination))))

(defn find-oxygen [program]
  (let [empty-state {:seen {}, :destinations empty-queue, :continue #{OPEN}}
        initial-state (update-exploration empty-state
                                          {:position [0 0], :distance 0}
                                          {:result OPEN, :program-state {:memory program, :pc 0, :rel-base 0}})]
    (:oxygen (first (drop-while (complement :oxygen)
                                (iterate explore initial-state))))))

(defn fill-with-oxygen [destination]
  (let [empty-state {:seen {}, :destinations empty-queue, :continue #{OPEN OXYGEN}}
        initial-state (update-exploration empty-state
                                          (assoc destination :distance 0)
                                          (assoc (select-keys destination [:program-state])
                                                 :result OXYGEN))]
    (:distance
     (first (drop-while #(seq (:destinations %))
                        (iterate explore initial-state))))))

(defn -main []
  (let [instructions (parse-file "input.txt")
        {:keys [distance] :as destination} (find-oxygen instructions)]
    (println distance)
    (println (fill-with-oxygen destination))))
