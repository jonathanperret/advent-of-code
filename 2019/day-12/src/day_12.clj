(ns day-12
  (:require
   [clojure.math.combinatorics :as combo]
   [clojure.java.io :as io]
   [util.collections :refer [find-period]]
   [util.numeric :refer [lcm]]))

(defn parse-moon [s]
  {:pos (map #(Integer/parseInt %) (next (re-find #"<x=(-?[0-9]+), y=(-?[0-9]+), z=(-?[0-9]+)>" s)))
   :vel [0 0 0]})

(defn gravity
  "Gravity that moon o exerts on moon m"
  [m o]
  (map compare (:pos o) (:pos m)))

(defn add-vec [& vecs]
  (apply map (fn [& components] (apply + components)) vecs))

(defn apply-gravity'
  "Updates moon m's velocity according to gravity exerted by moons os"
  [m os]
  (update m :vel (fn [v] (apply add-vec v (map #(gravity m %) os)))))

(defn apply-gravity
  "updates velocity of all moons according to gravity"
  [ms]
  (map #(apply-gravity' % ms) ms))

(defn apply-velocity'
  [m]
  (update m :pos add-vec (:vel m)))

(defn apply-velocity
  [ms]
  (map apply-velocity' ms))

(defn step
  [ms]
  (apply-velocity (apply-gravity ms)))

(defn energy
  [f m]
  (apply + (map #(Math/abs %) (f m))))

(defn total-energy
  [m]
  (* (energy :pos m) (energy :vel m)))

(defn system-energy [ms]
  (apply + (map total-energy ms)))

(defn project [ms axis]
  (map (fn [{:keys [pos vel]}]
         {:pos [(nth pos axis)]
          :vel [(nth vel axis)]})
       ms))

(defn axis-period [ms axis]
  (find-period (iterate step (project ms axis))))

(defn period [ms]
  (let [[_ x] (axis-period ms 0)
        [_ y] (axis-period ms 1)
        [_ z] (axis-period ms 2)]
    (lcm x y z)))

(defn -main []
  (with-open [f (io/reader "input.txt")]
    (let [ms (map parse-moon (line-seq f))]
      (println (system-energy (nth (iterate step ms) 1000)))
      (println (period ms)))))
