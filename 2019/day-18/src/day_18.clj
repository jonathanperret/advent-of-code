(ns day-18
  (:require
   [clojure.pprint :refer [pprint]]
   [util.explore :as explore]
   [util.map :as map]))

;;; Parse the map
(defn parse-char [c]
  (cond
    (= c \@)                  [:robot 0]
    (= c \#)                  [:wall]
    (Character/isUpperCase c) [:door c]
    (Character/isLowerCase c) [:key c]
    :else                     nil))

(def parse-options
  {:char parse-char
   :collect map/coord-to-char-map})

;;; 1st level exploration: use Dijkstra's algorithm to condense map into higher level graph
(defn tile-type-at [m coords]
  (first (m coords)))

(defn node? [m coords init]
  (when-not (= coords init)
    (#{:key :door} (tile-type-at m coords))))

(defn neighbours [m coords init]
  (when-not (node? m coords init)
    (for [pos (map/neighbours-4 coords)
          :when (not= :wall (tile-type-at m pos))]
      {:pos pos})))

(defn distances-from [m init]
  (into {}
        (map (fn [[coords {:keys [cost]}]] [(m coords) cost]))
        (last (explore/reachable-goals init #(node? m % init) #(neighbours m % init)))))

(defn abstract-graph [m]
  (into {}
        (keep (fn [[coords [type :as object]]]
                (cond (#{:robot :key :door} type)
                  [object (distances-from m coords)])))
        m))

;;; 2nd level exploration: find key order
(defn into-with
  ([f m coll] (into-with f m (map identity) coll))
  ([f m xform coll]
   (transduce xform
              (completing
               (fn [m [k v]]
                 (update m k #(cond->> v (some? %) (f %)))))
              m
              coll)))

(defn merge-paths [costs k base k-costs]
  (dissoc
   (into-with min
              costs
              (map (fn [[pos cost]] [pos (+ cost base)]))
              k-costs)
   k))

(defn export-to-reachable
  "Update the nodes of `m` that can be reached from `node`,
  adding paths to all other nodes that can be reached from `node`
  (assuming no cheaper path already exists), i.e., add nodes that
  can be transitively reached through `node`."
  [m node]
  (let [node-costs (m node)]
    (reduce-kv (fn [m pos base]
                 (update m pos merge-paths node base (dissoc node-costs pos)))
               m
               node-costs)))

(defn export-to-robots [m robot-count node]
  (let [node-costs (m node)]
    (reduce
     (fn [m r]
       (let [pos [:robot r]
             base (get-in m [pos node])]
         (cond-> m
           base (update pos merge-paths node base (dissoc node-costs pos)))))
     m
     (range robot-count))))

(defn export-paths [m robot-count node]
  (-> m
      (export-to-reachable node)
      (export-to-robots robot-count node)))

(defn move-robot [m robot k]
  (assoc m [:robot robot] (m [:key k])))

(defn grab-key [m robot-count robot k]
  (let [key [:key k]
        door [:door (Character/toUpperCase k)]
        entry (m key)]
    (-> m
        (export-paths robot-count door)
        (export-paths robot-count key)
        (move-robot robot k)
        (dissoc key door))))

(defn reachable-keys [m robot-count]
  (for [r (range robot-count)
        [[type k] cost] (m [:robot r])
        :when (= :key type)]
    {:key k, :cost cost, :robot r}))

(defn next-nodes-to-explore [m robot-count]
  (for [{:keys [key cost robot]} (reachable-keys m robot-count)]
    {:pos (grab-key m robot-count robot key), :cost cost}))

(defn no-more-keys? [m robot-count]
  (empty? (reachable-keys m robot-count)))

(defn cheapest-tour [abstract-graph robot-count]
  (explore/a* abstract-graph
              #(no-more-keys? % robot-count)
              #(next-nodes-to-explore % robot-count)))

;;; Part 2 - patch the map
(defn collect-map-patched [m]
  (reduce (fn [m [[x y :as coords] [type :as object]]]
            (if (not= type :robot)
              (assoc m coords object)
              (assoc m
                     [(dec x) (dec y)] [:robot 0], [x (dec y)] [:wall], [(inc x) (dec y)] [:robot 1]
                     [(dec x)      y ] [:wall],       [x      y ] [:wall], [(inc x)      y ] [:wall]
                     [(dec x) (inc y)] [:robot 2], [x (inc y)] [:wall], [(inc x) (inc y)] [:robot 3])))
          {}
          m))

(def parse-patched-options
  {:char parse-char
   :collect collect-map-patched})

;;; Main
(defn format-path [{:keys [path]}]
  (for [p path]
    (into {}
          (for [[[type r] reachable] p :when (= type :robot)
                [[type k] cost] reachable :when (= type :key)]
            [k {:cost cost, :robot r}]))))

(defn -main []
  (pprint (:cost (cheapest-tour (abstract-graph (map/read parse-options)) 1)))
  (pprint (:cost (cheapest-tour (abstract-graph (map/read parse-patched-options)) 4))))
