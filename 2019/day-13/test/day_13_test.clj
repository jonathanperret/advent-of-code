(ns day-13-test
  (:require [clojure.test :refer [deftest is testing]]
            [day-13 :refer :all]))

(deftest test-bounding-box
  (is (= [[-3 0] [5 2]]
         (bounding-box {[-1 1] nil
                        [5 0] nil
                        [-3 2] nil}))))
