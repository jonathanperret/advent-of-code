(ns day-13
  (:require
   [clojure.string :as string]
   [intcode :refer [parse-file IO run]]
   [lanterna.terminal :as t]))

(defn score [state val] ((-> state :fns :score) state val))

(defn paint [state x y tile] ((-> state :fns :paint) state x y tile))

(defn erase [state x y] ((-> state :fns :erase) state x y))

(defn direction [state] ((-> state :fns :direction) state))

(defn merge-commands [f1 f2]
  (fn [state & args]
    (apply f1 (apply f2 state args) args)))

(defn merge-querries [f1 f2]
  (fn [state]
    (let [v (f2 state)] ; Force eager evaluation
      (or (f1 state) v))))

(defn merge-fns [old new]
  (into old
        (map (fn [[name f]]
               (let [wrapped (name old)
                     merge (if (= :direction name) merge-querries merge-commands)]
                 [name (merge f wrapped)])))
        new))

(defn mix-in [state & {:as fns}]
  (update state :fns merge-fns fns))

;; Plain terminal UI
(def tile-char {nil \ , :wall \█, :block \#, :paddle \-, :ball \o})

(defn bounding-box [screen]
  (let [coordinates (keys screen)
        xs (map first coordinates)
        ys (map fnext coordinates)]
    [[(apply min xs) (apply min ys)] [(apply max xs) (apply max ys)]]))

(defn render-screen [screen]
  (let [[[xmin ymin] [xmax ymax]] (bounding-box screen)]
    (for [y (range ymin (inc ymax))]
      (apply str
             (for [x (range xmin (inc xmax))]
               (tile-char (screen [x y])))))))

(defn render-terminal [{:keys [score screen] :as terminal}]
  (cons (str "Score: " score)
        (render-screen screen)))

(defn print-state [{:keys [terminal]}]
  (println (string/join \newline (render-terminal terminal))))

(defn with-terminal [state]
  (mix-in (assoc state :terminal {:screen {}})
          :score (fn [this score] (assoc-in this [:terminal :score] score))
          :paint (fn [this x y tile] (update-in this [:terminal :screen] assoc [x y] tile))
          :erase (fn [this x y] (update-in this [:terminal :screen] dissoc [x y]))
          :direction (fn [this] (print-state this))))

;; Lanterna UI
(defn with-lanterna [state term]
  (mix-in state
          :score (fn [this score]
                   (doto term
                     (t/move-cursor 0 0)
                     (t/put-string "Score: ")
                     (t/put-string (str score)))
                   this)
          :paint (fn [this x y tile]
                   (doto term
                     (t/move-cursor x (inc y))
                     (t/put-character (tile-char tile)))
                   this)
          :erase (fn [this x y]
                   (doto term
                     (t/move-cursor x (inc y))
                     (t/put-character \ ))
                   this)))

;; Block counter
(defn with-block-counter [state]
  (mix-in (assoc state :block-count 0)
          :paint (fn [this _ _ tile] (update this :block-count inc))))

;; AI player
(defn with-ai [state]
  (mix-in (assoc state :ai {})
          :paint (fn [this x _ tile] (cond-> this (#{:paddle :ball} tile) (assoc-in [:ai tile] x)))
          :direction (fn [{{:keys [ball paddle]} :ai}] (compare ball paddle))))

;; Arcade cabinet
(def kind {1 :wall, 2 :block, 3 :paddle, 4 :ball})

(defn paint-x [state x]
  (assoc state :x x))

(defn paint-y [state y]
  (assoc state :y y))

(defn paint-tile [{:keys [x y] :as state} tile]
  (cond
    (= [-1 0] [x y]) (score state tile)
    (zero? tile) (erase state x y)
    :else (paint state x y (kind tile))))

(defn nop [state & args] state)

(def empty-state {:mode paint-x
                  :fns {:score nop, :paint nop, :erase nop, :direction (constantly nil)}})

(def next-mode {paint-x paint-y, paint-y paint-tile, paint-tile paint-x})

(defn update-state [{:keys [mode] :as state} v]
  (-> state (mode v) (assoc :mode (next-mode mode))))

(defn cabinet [init-state]
  (let [state (volatile! init-state)
        io (reify IO
             (in [_] (direction @state))
             (out [_ v] (vswap! state update-state v)))]
    [state io]))

(defn state-after [init-state instructions]
  (let [[state io] (cabinet init-state)]
    (run instructions io)
    @state))

(defn run-term
  []
  (let [instructions (parse-file "input.txt")
        init (-> empty-state with-terminal with-block-counter with-ai)]
    (println "part 1")
    (let [state (state-after init instructions)]
      (println "Blocks:" (:block-count state))
      (print-state state))
    (println "part 2")
    (let [state (state-after init (assoc instructions 0 2))]
      (print-state state))))

(defn run-lanterna
  []
  (let [instructions (parse-file "input.txt")
        term (t/get-terminal :text)
        init (-> empty-state (with-lanterna term) with-block-counter with-ai)]
    (t/in-terminal term
      (let [state (state-after init instructions)]
        (doto term
          (t/move-cursor 0 0)
          (t/put-string "Blocks: ")
          (t/put-string (str (:block-count state)))
          (t/get-key-blocking)))
      (state-after init (assoc instructions 0 2))
      (t/get-key-blocking term))))

(defn -main
  []
  (run-lanterna))
