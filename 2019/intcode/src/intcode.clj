(ns intcode
  (:refer-clojure :exclude [read])
  (:require
   [clojure.core.async :as async :refer [chan <!! >!! close!]]
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [clojure.string :as string]))

(defn parse-line [line]
  (into []
        (map edn/read-string)
        (string/split line #",")))

(defn parse-file [name]
  (with-open [input (io/reader name)]
    (parse-line (first (line-seq input)))))

;;; Program input output
(defprotocol IO
  (in [this])
  (out [this val]))

(defn printing-io [i]
  (reify IO (in [_] i) (out [_ v] (println v))))

;;; Parameter modes
(defn parameter-mode [opcode]
  (map :mode (rest (iterate (fn [{:keys [modes]}] {:mode (mod modes 10) :modes (quot modes 10)}) {:modes (quot opcode 100)}))))

(defprotocol ParameterMode
  (read [_ state])
  (address [_ state]))

(defn read-memory [mode {:keys [memory] :as state}]
  (get memory (address mode state) 0))

(defn write [mode state val]
  (let [mem-size (count (:memory state))
        pos (address mode state)]
    (cond-> state
      (> pos mem-size) (update :memory into (repeat (- pos mem-size) 0))
      :always          (update :memory assoc pos val))))

(defn position-mode [pos]
  (reify ParameterMode
    (read [this state] (read-memory this state))
    (address [_ _] pos)))

(defn immediate-mode [i]
  (reify ParameterMode
    (read [_ _] i)))

(defn relative-mode [offset]
  (reify ParameterMode
    (read [this state] (read-memory this state))
    (address [_ {:keys [rel-base]}] (+ rel-base offset))))

(def parameter-modes [position-mode immediate-mode relative-mode])

(defn read-access [state args f]
  (apply f (map #(read % state) args)))

(defn write-access [state args f]
  (write (peek args) state (read-access state (butlast args) f)))

;;; Operation categories

(defn alu [op]
  #(write-access %1 %2 op))

(defn input [{:keys [io] :as state} args]
  (write-access state args #(in io)))

(defn output [{:keys [io] :as state} args]
  (read-access state args #(out io %))
  state)

(defn halt [state _]
  (assoc state :done true))

(defn jump [op]
  (fn [state args]
    (read-access state args (fn [cond pc]
                              (if (op 0 cond)
                                (assoc state :pc pc)
                                state)))))

(defn adjust-rel-base [state args]
  (read-access state args #(update state :rel-base + %)))

(defn cmp [op]
  #(if (op %1 %2) 1 0))

;;;
(def operations
  {1 {:count 4, :instr (alu +')}
   2 {:count 4, :instr (alu *')}
   3 {:count 2, :instr input}
   4 {:count 2, :instr output}
   5 {:count 3, :instr (jump not=)}
   6 {:count 3, :instr (jump =)}
   7 {:count 4, :instr (alu (cmp <))}
   8 {:count 4, :instr (alu (cmp =))}
   9 {:count 2, :instr adjust-rel-base}
   99 {:count 1, :instr halt}})

(defn decode [program pos]
  (let [opcode (program pos)
        {:keys [count] :as op} (operations (mod opcode 100))
        modes (map parameter-modes (parameter-mode opcode))
        mode-args (subvec program (inc pos) (+ pos count))]
    (assoc op :args (mapv #(%1 %2) modes mode-args))))


;;; Execution
(defn step [state]
  (let [{:keys [count instr args]} (decode (:memory state) (:pc state))]
    (-> state
        (update :pc + count)
        (instr args))))

(defn run
  [program io]
  (:memory (first (drop-while (complement :done) (iterate step {:memory program, :io io, :pc 0, :rel-base 0})))))
