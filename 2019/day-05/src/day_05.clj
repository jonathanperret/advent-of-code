(ns day-05
  (:require
   [intcode :refer [parse-file printing-io run]]))

(defn -main
  []
  (let [instructions (parse-file "input.txt")]
    (println "part 1")
    (run instructions (printing-io 1))
    (println "part 2")
    (run instructions (printing-io 5))))
