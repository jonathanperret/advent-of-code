(ns day-14
  (:require
   [clojure.java.io :as io]
   [instaparse.core :as insta]))

(def reactions
  (insta/parser
   "reactions = (reaction <newline>)+
    reaction = inputs <arrow> output
    inputs = reagent-quantity (<comma> reagent-quantity)*
    output = reagent-quantity
    reagent-quantity = quantity <whitespace> reagent
    <quantity> = #'[0-9]+'
    <reagent> = #'[A-Z]+'
    comma = ',' whitespace
    arrow = whitespace '=>' whitespace
    newline = '\n'
    whitespace = #' *'
    "))

(defn to-map [& kvs] (into {} kvs))

(defn fixup-reactions [tree]
  (insta/transform
   {:reagent-quantity (fn [quantity name] [name (Integer/parseInt quantity)])
    :inputs to-map
    :output identity
    :reaction (fn [inputs [type quantity]] [type {:inputs inputs :quantity quantity}])
    :reactions to-map}
   tree))

(defn parse-input [i]
  (fixup-reactions (reactions i)))

(defn batch-count [required batch-size]
  (quot (+ required batch-size -1) batch-size))

(defn scale [count batch]
  (into {}
        (map (fn [[k v]] [k (* count v)]))
        batch))

(defn add-batch [b1 b2]
  (merge-with + b1 b2))

(def +nil (fnil + 0 0))

(defn stash-ore [{{:strs [ORE]} :required :as state}]
  (-> state
      (update :ore +nil ORE)
      (update :required dissoc "ORE")))

(defn reduce-requirements [{:keys [required reactions available] :or {available {}} :as state}]
  (let [[[name count]] (seq required)
        {:keys [inputs quantity]} (reactions name)
        from-storage (min count (or (available name) 0))
        missing (- count from-storage)
        batches (batch-count missing quantity)
        surplus (- (* batches quantity) missing)]
    (cond-> state
        :always (update :required dissoc name)
        (pos? missing)(update :required add-batch (scale batches inputs))
        (pos? surplus) (update-in [:available name] +nil surplus)
        (pos? from-storage) (update-in [:available name] - from-storage)
        :always stash-ore)))

(defn fully-reduce [state]
  (first (drop-while #(seq (:required %)) (iterate reduce-requirements state))))

(defn ore-and-fuel [reactions fuel]
  (assoc (select-keys (fully-reduce {:required {"FUEL" fuel} :reactions reactions}) [:ore])
         :fuel fuel))

(defn fuel-bounds [reactions ore-per-fuel ore]
  (first (drop-while #(>= ore (-> % :high :ore))
                     (iterate (fn [{:keys [low high]}]
                                {:low high, :high (ore-and-fuel reactions (* (:fuel high) 8))})
                              {:high (ore-and-fuel reactions (quot ore ore-per-fuel))}))))

(defn fuel-spread [{:keys [low high]}]
  (- (:fuel high) (:fuel low)))

(defn nudge [mid low high]
  (cond-> mid (= low mid) inc (= high mid) dec))

(defn interpolate [bounds ore]
  (let [{{lf :fuel, lo :ore} :low,
         {hf :fuel, ho :ore} :high} bounds]
    (-> (+ lf (int (quot (*' (- ore lo) (- hf lf))
                         (- ho lo))))
        (nudge lf hf))))

(defn fuel-for-ore [reactions ore-per-fuel ore]
  (-> (first (drop-while #(and (> (fuel-spread %) 1)
                               (not= (-> % :low :ore) ore))
                         (iterate #(let [mid (ore-and-fuel reactions (interpolate % ore))
                                         pos (if (< ore (:ore mid)) :high :low)]
                                     (assoc % pos mid))
                                  (fuel-bounds reactions ore-per-fuel ore))))
      :low :fuel))

(defn -main []
  (let [reactions (parse-input (slurp "input.txt"))
        unit-ore (:ore (ore-and-fuel reactions 1))]
    (println unit-ore)
    (println (fuel-for-ore reactions unit-ore 1000000000000))))
