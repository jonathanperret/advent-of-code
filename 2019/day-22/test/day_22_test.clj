(ns day-22-test
  (:require
   [clojure.string :as string]
   [clojure.test :refer [deftest is testing]]
   [day-22 :refer :all]))

(deftest test-parse-input
  (is (= [[:deal-new] [:cut 3] [:cut -4] [:deal-increment 3]]
         (parse-input "deal into new stack\ncut 3\ncut -4\ndeal with increment 3\n"))))

(defn trace [dir shuffle]
  (let [count 10]
    (map #(dir shuffle % count) (range count))))

(deftest test-deal-new
  (testing "shuffle"
          ; 0 1 2 3 4 5 6 7 8 9
    (is (= [9 8 7 6 5 4 3 2 1 0] (trace shuffle [:deal-new]))))
  (testing "rev"
          ; 0 1 2 3 4 5 6 7 8 9
    (is (= [9 8 7 6 5 4 3 2 1 0] (trace rev [:deal-new])))))

(deftest test-shuffle-cut
  (testing "shuffle"
          ; 0 1 2 3 4 5 6 7 8 9
    (is (= [7 8 9 0 1 2 3 4 5 6] (trace shuffle [:cut 3])))
    (is (= [4 5 6 7 8 9 0 1 2 3] (trace shuffle [:cut -4]))))
  (testing "rev"
          ; 0 1 2 3 4 5 6 7 8 9
    (is (= [3 4 5 6 7 8 9 0 1 2] (trace rev [:cut 3])))
    (is (= [6 7 8 9 0 1 2 3 4 5] (trace rev [:cut -4])))))

(deftest test-deal-increment
  (testing "shuffle"
          ; 0 1 2 3 4 5 6 7 8 9
    (is (= [0 3 6 9 2 5 8 1 4 7] (trace shuffle [:deal-increment 3])))
    (is (= [0 7 4 1 8 5 2 9 6 3] (trace shuffle [:deal-increment 7]))))
  (testing "rev"
          ; 0 1 2 3 4 5 6 7 8 9
    (is (= [0 7 4 1 8 5 2 9 6 3] (trace rev [:deal-increment 3])))
    (is (= [0 3 6 9 2 5 8 1 4 7] (trace rev [:deal-increment 7])))))

(defn shuffles [& specs]
  (parse-input
   (string/join (map #(str % "\n") specs))))

(deftest test-combine-shuffles
  (testing "shuffle"
          ; 0 1 2 3 4 5 6 7 8 9
    (is (= [1 4 7 0 3 6 9 2 5 8] (trace apply-shuffles (simplify (shuffles "cut 6" "deal with increment 7" "deal into new stack") 10)))))
  (testing "rev"
          ; 0 1 2 3 4 5 6 7 8 9
    (is (= [3 0 7 4 1 8 5 2 9 6] (trace rev-shuffles (simplify (shuffles "cut 6" "deal with increment 7" "deal into new stack") 10))))))

(deftest testing-algebraic-properties
  (testing "cuts add (modulo deck count)"
    (is (= (trace apply-shuffles (shuffles "cut 3" "cut 4"))
           (trace apply-shuffles (shuffles "cut 7"))))
    (is (= (trace apply-shuffles (shuffles "cut 5" "cut 6"))
           (trace apply-shuffles (shuffles "cut 1")))))

  (testing "deals with increment multiply (modulo deck count)"
    (is (= (trace apply-shuffles (shuffles "deal with increment 3" "deal with increment 3"))
           (trace apply-shuffles (shuffles "deal with increment 9"))))
    (is (= (trace apply-shuffles (shuffles "deal with increment 7" "deal with increment 3"))
           (trace apply-shuffles (shuffles "deal with increment 21"))
           (trace apply-shuffles (shuffles)))))

  (testing "cut and deal with increment commute (multiplying the cut)"
    (is (= (trace apply-shuffles (shuffles "deal with increment 3" "cut 6"))
           (trace apply-shuffles (shuffles "cut 2" "deal with increment 3")))))

  (testing "deal into new stack is really just deal with increment -1 (and a cut)"
    (is (= (trace apply-shuffles (shuffles "deal into new stack"))
           (trace apply-shuffles (shuffles "deal with increment 9" "cut 1"))))))

(deftest test-simplify
  (is (= [] (simplify [] 10)))

  (is (= [[:deal-increment 9][:cut 1]] (simplify [[:deal-new]] 10)))
  (is (= [] (simplify [[:deal-new] [:deal-new]] 10)))

  (is (= [[:deal-increment 9]] (simplify [[:deal-increment 3] [:deal-increment 3]] 10)))
  (is (= [] (simplify [[:deal-increment 7] [:deal-increment 3]] 10)))

  (is (= [[:cut 3]] (simplify [[:cut 7] [:cut 6]] 10)))
  (is (= [] (simplify [[:cut 7] [:cut 3]] 10)))

  (is (= [[:deal-increment 9] [:cut 8]] (simplify [[:cut 3] [:deal-new]] 10)))

  (is (= [[:deal-increment 3] [:cut 6]] (simplify [[:cut 2] [:deal-increment 3]] 10)))
  (is (= [[:deal-increment 7] [:cut 4]] (simplify [[:cut 2] [:deal-increment 7]] 10)))

  (is (= [[:deal-increment 7] [:cut 1]] (simplify [[:deal-increment 3] [:deal-new]] 10)))

  (is (= [[:deal-increment 9] [:cut 6]] (simplify [[:deal-increment 3] [:cut 2] [:deal-increment 3]] 10))))

(deftest test-pow
  (let [s (simplify (shuffles "cut 6" "deal with increment 7" "deal into new stack") 10)]
    (is (= (simplify (apply concat (repeat 1278531 s)) 119315717514047)
           (pow s 1278531 119315717514047)))))
