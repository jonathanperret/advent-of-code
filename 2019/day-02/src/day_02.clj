(ns day-02
  (:require
   [intcode :refer [parse-file run]]))

(defn output
  [program noun verb]
  (-> program
      (assoc 1 noun, 2 verb)
      (run nil)
      (get 0)))

(defn noun-verb-producing [program expected]
  (some
   identity
   (for [noun (range 100)
         verb (range 100)]
     (when (= expected (output program noun verb)) (+ (* 100 noun) verb)))))

(defn -main
  []
  (let [instructions (parse-file "input.txt")]
    (println (output instructions 12 2))
    (println (noun-verb-producing instructions 19690720))))
