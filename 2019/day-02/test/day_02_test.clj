(ns day-02-test
  (:require
   [day-02 :refer :all]
   [clojure.test :refer [deftest is testing]]))

(deftest noun-verb-producting-test
  (with-redefs [output (fn [program noun verb]
                         (is (= "program" program))
                         (if (= [5 43] [noun verb])
                           :match
                           :mismatch))]
    (is (= 543 (noun-verb-producing "program" :match)))))
