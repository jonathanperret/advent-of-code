(ns day-09
  (:require
   [clojure.math.combinatorics :as combo]
   [common]))

(def input (->> (common/input) (common/lines) (map read-string)))

(defn sums-of-pairs [numbers]
  (into #{}
        (map #(apply + %))
        (combo/combinations numbers 2)))

(defn valid-sums []
  (->> input
       (partition 25 1)
       (map sums-of-pairs)))

(defn invalid-input []
  (first
   (keep (fn [[acceptable n]] (when-not (acceptable n) n))
         (map vector (valid-sums) (drop 25 input)))))

(def running-totals
  (vec (reductions + 0 input)))

(defn sums-of-length [n]
  (into {}
        (for [i (range (- (count running-totals) n))]
          [(- (running-totals (+ i n))
              (running-totals i))
           {:start i, :end (+ i n)}])))

(defn matching-range []
  (let [invalid-input (invalid-input)]
    (first
     (keep #(% invalid-input)
           (map #(sums-of-length (+ 2 %)) (range))))))

(defn matching-range-stats []
  (let [{:keys [start end]} (matching-range)
        range (subvec (vec input) start end)
        smallest (apply min range)
        largest (apply max range)]
    (+ smallest largest)))

(defn -main []
  (println (time (invalid-input)))
  (println (time (matching-range-stats))))
