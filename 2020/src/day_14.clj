(ns day-14
  (:require
   [instaparse.core :as insta]
   [common]))

(def prog-parser
  (insta/parser
   "<lines> = line*
    <line>    = (setmask | setmem) <'\n'>
    setmask = <'mask = '> mask
    mask    = #'[01X]+'
    setmem  = <'mem['> addr <'] = '> val
    addr    = number
    val     = number
    number  = #'[0-9]+'"))

(defn mapchar [m s]
  (apply str (map #(or (m %) %) s)))

(defn parse-bin [m s]
  (Long/parseLong (mapchar m s) 2))

(defn floaters [xes]
  (keep-indexed #(when (= %2 \X) %1)
                (reverse xes)))

(defn float-bit [vals n]
  (mapcat (fn [v] [(bit-clear v n) (bit-set v n)]) vals))

(defn decode-xes [xes]
  (let [fs (floaters xes)]
    #(reduce float-bit [%] fs)))

(def input
  (->> (common/input)
       slurp
       prog-parser
       (insta/transform
        {:number read-string
         :mask #(hash-map
                 :ones   (parse-bin {\X \0} %)
                 :zeroes (parse-bin {\X \1} %)
                 :xes    (decode-xes %))
         :setmem  #(hash-map :op :setmem,  :args (into {} %&))
         :setmask #(hash-map :op :setmask, :args %1)})))

(def ops-1
  {:setmask (fn [state args] (assoc  state :mask args))
   :setmem  (fn [{{:keys [ones zeroes]} :mask :as state}
                 {:keys [addr val]}]
              (update state :mem assoc addr (-> val (bit-or ones) (bit-and zeroes))))})

(defn run [ops]
  (reduce
   (fn [state {:keys [op args]}]
     ((ops op) state args))
   {:mem {}}
   input))

(defn total [state]
  (->> state :mem vals (apply +)))

(defn part-1 []
  (total (run ops-1)))

(def ops-2
  {:setmask (fn [state args] (assoc  state :mask args))
   :setmem  (fn [{{:keys [ones xes]} :mask :as state}
                 {:keys [addr val]}]
              (update state :mem (fn [mem]
                                   (reduce #(assoc %1 %2 val)
                                           mem
                                           (-> addr (bit-or ones) xes)))))})

(defn part-2 []
  (total (run ops-2)))

(defn -main []
  (println (time (part-1)))
  (println (time (part-2))))
