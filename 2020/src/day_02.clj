(ns day-02
  (:require
   [instaparse.core :as insta]

   [common]))

(def parse-passwords
  (insta/parser
   "<passwords>   = (password-line <'\n'>)*
    password-line = policy <': '> password
    policy        = min <'-'> max <' '> letter
    min           = number
    max           = number
    letter        = #'[a-z]'
    number        = #'[0-9]+'
    password      = #'[a-z]+'"))

(defn mapify-passwords [parsed-passwords]
  (insta/transform
   {:number        read-string
    :letter        (fn [[l]] [:letter l])
    :policy        (fn [& args] [:policy (into {} args)])
    :password-line #(into {} %&)}
   parsed-passwords))

(defn read-passwords [input]
  (-> input parse-passwords mapify-passwords))

(defn match-count [haystack needle?]
  (->> haystack (filter needle?) count))

(defn valid-sled? [{{:keys [min max letter]} :policy, :keys [password]}]
  (<= min (match-count password #{letter}) max))

(defn valid-toboggan? [{{:keys [min max letter]} :policy, :keys [password]}]
  (let [ctrl (map #(get password (dec %)) [min max])]
    (= 1 (match-count ctrl #{letter}))))

(defn -main
  []
  (let [passwords (-> (common/input) slurp read-passwords)]
    (println (time (match-count passwords valid-sled?)))
    (println (time (match-count passwords valid-toboggan?)))))
