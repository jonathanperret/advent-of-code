(ns day-11
  (:require
   [clojure.set :as set]
   [common]))

(def input (->> (common/input) (common/lines)))
(def height (count input))
(def width (count (first input)))

(defn debug [{:keys [empty occupied]}]
  (let [format (fn [seat]
                 (cond
                   (empty seat)    \L
                   (occupied seat) \#
                   :else           \.))]
    (for [row (range height)]
      (->> (for [col (range width)]
             (format [row col]))
           (apply str)))))

(def seats
  (set
   (mapcat
    (fn [r line]
      (keep-indexed
       (fn [c cell]
         (when (= cell \L)
           [r c]))
       line))
    (range)
    input)))

(defn around [down stay up [row col]]
  [[(down row) (down col)] [(down row) (stay col)] [(down row) (up col)]
   [(stay row) (down col)]                         [(stay row) (up col)]
   [(up   row) (down col)] [(up   row) (stay col)] [(up   row) (up col)]])

(defn adjacent-seats [seat]
  (around dec identity inc seat))

(defn succ [neighbourhood too-full {:keys [empty occupied]}]
  (let [neighbour-count #(->> % neighbourhood (keep occupied) count)

        newly-occupied (->> empty    (filter #(zero? (neighbour-count %)         )) set)
        newly-empty    (->> occupied (filter #(>=    (neighbour-count %) too-full)) set)]

    {:empty    (-> empty    (set/difference newly-occupied) (set/union newly-empty))
     :occupied (-> occupied (set/difference newly-empty)    (set/union newly-occupied))}))

(defn fixed-point
  [f x0]
  (->> x0
       (iterate f)
       (partition 2 1)
       (drop-while #(apply not= %))
       ffirst))

(defn stable-configuration-count [neighbourhood too-full]
  (->> {:occupied #{} :empty seats}
       (fixed-point #(succ neighbourhood too-full %))
       :occupied
       count))

(defn part-1 []
  (stable-configuration-count adjacent-seats 4))

(defn find-watched-seats [seat]
  (keep #(->> %
              (apply map vector)                    ; Turn pair of lists into list of pairs
              (keep seats)
              first)
        (around #(range (dec %) -1 -1)
                repeat
                #(range (inc %) (max height width)) ; Because of max we can overshoot but no harm done
                seat)))

(def watched-seats
  (into {}
        (map (fn [seat] [seat (find-watched-seats seat)]))
        seats))

(defn part-2 []
  (stable-configuration-count watched-seats 5))

(defn -main []
  (println (time (part-1)))
  (println (time (part-2))))
