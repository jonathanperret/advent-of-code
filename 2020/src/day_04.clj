(ns day-04
  (:require
   [clojure.spec.alpha :as s]
   [instaparse.core :as insta]

   [common]))


;;; Physical parsing: list of records of key-value pairs

(def passport-parser
  (insta/parser
   "<passports>  = passport (<passport-sep> passport)*

    passport-sep = '\n'
    passport     = key-value (<kv-sep> key-value)* <'\n'>
    kv-sep       = #'[ \n]'
    key-value    = key <':'> value
    key          = #'[a-z]+'
    value        = #'[^ \n]+'"))

(defn parse-passports [s]
  (->> s
   passport-parser
   (insta/transform
    {:key       keyword
     :value     identity
     :key-value vector
     :passport  #(into {} %&)})))


;;; Logical parsing of fields

(defn parse-if-string [s parser]
  (cond-> s (string? s) parser))

;; Numeric field type
(defn parse-num [n]
  (parse-if-string n
    #(try
       (Long/parseLong %)
       (catch Exception _
         ::s/invalid))))

(s/def ::numeric (s/and (s/conformer parse-num) int?))


;; Measurement field type: a unit with a value
(def measurement-parser
  (insta/parser
   "measurement = value unit

    value       = #'[0-9]+'
    unit        = #'[a-z]+'"))

(defn ->measurement [m]
  (->> m
       (insta/transform
        {:value       (fn [v] [:value (Long/parseLong v)])
         :measurement #(into {} %&)})))

(defn parse-measurement [n]
  (parse-if-string n
    #(let [res (measurement-parser %)]
       (if (insta/failure? res)
         ::s/invalid
         (->measurement res)))))

(s/def ::value int?)

(s/def ::unit string?)

(s/def ::measurement (s/and (s/conformer parse-measurement)
                            (s/keys :req-un [::value ::unit])))


;; Passport fields
(s/def ::byr (s/and ::numeric
                    #(<= 1920 % 2002)))

(s/def ::iyr (s/and ::numeric
                    #(<= 2010 % 2020)))

(s/def ::eyr (s/and ::numeric
                    #(<= 2020 % 2030)))

(s/def ::hgt (s/and ::measurement
                    (fn [{:keys [value unit]}]
                      (case unit
                       "cm" (<= 150 value 193)
                       "in" (<=  59 value  76)))))

(s/def ::hcl #(re-matches #"#[0-9a-f]{6}" %))

(s/def ::ecl #{"amb" "blu" "brn" "gry" "grn" "hzl" "oth"})

(s/def ::pid #(re-matches #"[0-9]{9}" %))

;;; Putting it all together

;; Only checks that the required fields are present
(s/def :unchecked/passport
  (s/keys :req-un [:unchecked/byr :unchecked/iyr :unchecked/eyr :unchecked/hgt :unchecked/hcl :unchecked/ecl :unchecked/pid]
          :opt-un [:unchecked/cid]))

;; Checks field presence and validates their values
(s/def ::passport
  (s/keys :req-un [::byr ::iyr ::eyr ::hgt ::hcl ::ecl ::pid]
          :opt-un [::cid]))

(defn count-valid [spec passports]
  (->> passports
       (filter #(s/valid? spec %))
       count))

(defn -main
  []
  (let [passports (-> (common/input) slurp parse-passports)]
    (println (time (count-valid :unchecked/passport passports)))
    (println (time (count-valid ::passport passports)))))
