(ns day-12
  (:require
   [common]))

(defn parse [i]
  (let [[_ action value] (re-find #"([A-Z])([0-9]+)" i)]
    [action
     (Integer/parseInt value)]))

(def input (->> (common/input) (common/lines) (map parse)))

(defn left [[dx dy]]
  [dy (- dx)])

(defn right [dir]
  (-> dir left left left))

(def rotations
  {"L" left
   "R" right})

(defn rotate [dir rotation angle]
  (reduce #(%2 %1) dir (repeat (/ angle 90) rotation)))

(def east [1 0])
(def north (rotate east left 90))

(def dirs
  {"N" north
   "E" east
   "S" (rotate east right 90)
   "W" (rotate north left 90)})

(defn translate [pos dir scale]
  (map +
       pos
       (map #(* % scale) dir)))

(defn step-1 [ship [action value]]
  (let [d (dirs action)
        r (rotations action)]
    (cond
      d     (update ship :pos translate d value)
      r     (update ship :dir rotate    r value)
      :else (update ship :pos translate (:dir ship) value))))

(defn manhattan [[x y]]
  (+ (Math/abs x) (Math/abs y)))

(defn part-1 []
  (->> input
       (reduce step-1 {:pos [0  0], :dir east})
       :pos
       manhattan))

(defn step-2 [ship [action value]]
  (let [d (dirs action)
        r (rotations action)]
    (cond
      d     (update ship :waypoint translate d                value)
      r     (update ship :waypoint rotate    r                value)
      :else (update ship :pos      translate (:waypoint ship) value))))

(defn part-2 []
  (->> input
       (reduce step-2 {:pos [0  0], :waypoint (-> [0 0] (translate east 10) (translate north 1))})
       :pos
       manhattan))

(defn -main []
  (println (time (part-1)))
  (println (time (part-2))))
