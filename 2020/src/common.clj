(ns common
  (:require
   [clojure.java.io :as io]))

(defmacro input []
  `(io/file "input" (-> ~*ns* ns-name (str ".txt"))))

(defn lines [name]
  (with-open [f (io/reader name)]
    (doall (line-seq f))))
