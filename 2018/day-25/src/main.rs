use day_25::*;

fn main() {
    let points = read_input().unwrap();
    println!("Constellations: {}", constellation_count(&points));
}
