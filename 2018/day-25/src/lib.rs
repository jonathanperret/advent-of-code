use commons::{lines, parse_input, Result};

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Point {
    x: i8,
    y: i8,
    z: i8,
    t: i8,
}

fn abs_diff(a: i8, b: i8) -> u8 {
    (i16::from(a) - i16::from(b)).abs() as u8
}

impl Point {
    fn dist(&self, other: &Self) -> u8 {
        abs_diff(self.x, other.x)
            + abs_diff(self.y, other.y)
            + abs_diff(self.z, other.z)
            + abs_diff(self.t, other.t)
    }

    pub fn belongs_to<'a, T>(&self, constellation: T) -> bool
    where
        T: IntoIterator<Item = &'a Self>,
    {
        constellation.into_iter().any(|b| self.dist(b) <= 3)
    }
}

pub fn constellation_count<'a, T>(points: T) -> usize
where
    T: IntoIterator<Item = &'a Point>,
{
    constellations(points).len()
}

pub fn constellations<'a, T>(points: T) -> Vec<Vec<Point>>
where
    T: IntoIterator<Item = &'a Point>,
{
    let mut result: Vec<Vec<Point>> = vec![];
    for &p in points.into_iter() {
        let mut new_result = vec![];
        let mut constellation = vec![p];
        for mut c in result {
            if p.belongs_to(c.iter()) {
                constellation.append(&mut c);
            } else {
                new_result.push(c);
            }
        }
        new_result.push(constellation);
        result = new_result;
    }
    result
}

pub fn read_input<'a>() -> Result<'a, Vec<Point>> {
    parse_input!(lines!(parser::point))
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;
    use maplit::hashset;
    use nom::types::CompleteStr;

    #[test]
    fn test_abs_diff() {
        assert_eq!(abs_diff(-3, 8), 11);
    }

    #[test]
    fn test_dist() {
        let a = Point {
            x: 1,
            y: -5,
            z: 18,
            t: 1,
        };
        let b = Point {
            x: 8,
            y: 17,
            z: -9,
            t: 0,
        };
        assert_eq!(7 + 22 + 27 + 1, a.dist(&b));
    }

    #[test]
    fn test_belongs_to() {
        let origin = Point {
            x: 0,
            y: 0,
            z: 0,
            t: 0,
        };
        assert!(origin.belongs_to(&hashset! {
            Point{x: 2, y:1, z: 0, t: 0},
            Point{x: 3, y:2, z: -1, t: 18}
        }));
        assert!(!origin.belongs_to(&hashset! {
            Point{x: 3, y:2, z: -1, t: 18},
            Point{x: 0, y: 0, z: 0, t: 42}
        }));
    }

    fn parse(i: &str) -> Vec<Point> {
        lines!(CompleteStr(i), parser::point).unwrap().1
    }

    fn constellation_count(i: &str) -> usize {
        super::constellation_count(&parse(i))
    }

    #[test]
    fn test_constellations() {
        assert_eq!(
            2,
            constellation_count(indoc!(
                "
                0,0,0,0
                3,0,0,0
                0,3,0,0
                0,0,3,0
                0,0,0,3
                0,0,0,6
                9,0,0,0
                12,0,0,0
                "
            ))
        );
        assert_eq!(
            4,
            constellation_count(indoc!(
                "
                -1,2,2,0
                0,0,2,-2
                0,0,0,-2
                -1,2,0,0
                -2,-2,-2,2
                3,0,2,-1
                -1,3,2,2
                -1,0,-1,0
                0,2,1,-2
                3,0,0,0
                "
            ))
        );
        assert_eq!(
            3,
            constellation_count(indoc!(
                "
                1,-1,0,1
                2,0,-1,0
                3,2,-1,0
                0,0,3,1
                0,0,-1,-1
                2,3,-2,0
                -2,2,0,0
                2,-2,0,-1
                1,-1,0,-1
                3,2,0,2
                "
            ))
        );
        assert_eq!(
            8,
            constellation_count(indoc!(
                "
                1,-1,-1,-2
                -2,-2,0,1
                0,2,1,3
                -2,3,-2,1
                0,2,3,-2
                -1,-1,1,-2
                0,-2,-1,0
                -2,2,3,-1
                1,2,2,0
                -1,-2,0,-2
                "
            ))
        );
    }
}

mod parser {
    use super::Point;
    use commons::parser::int;
    use nom::types::CompleteStr;
    use nom::{char, do_parse, IResult};

    #[rustfmt::skip]
    pub(super) fn point(i: CompleteStr) -> IResult<CompleteStr, Point> {
        do_parse!(i,
            x: int        >>
               char!(',') >>
            y: int        >>
               char!(',') >>
            z: int        >>
               char!(',') >>
            t: int        >>
            (Point{x, y , z, t})
        )
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_parse_point() {
            assert_eq!(
                Point {
                    x: 1,
                    y: -2,
                    z: 3,
                    t: -4
                },
                point(CompleteStr(&"1,-2,3,-4")).unwrap().1
            );
        }
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;

    #[test]
    fn test_constellation_count() {
        assert_eq!(310, constellation_count(&read_input().unwrap()));
    }
}
