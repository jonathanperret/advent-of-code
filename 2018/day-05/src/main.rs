use commons::{parse_input, Result};

mod parser {
    use nom::types::CompleteStr;
    use nom::{alpha, eol, map, named, terminated, IResult};

    named!(polymer<CompleteStr, String>,
           map!(alpha, |s| s.to_string())
    );

    pub(super) fn input(i: CompleteStr) -> IResult<CompleteStr, String> {
        terminated!(i, polymer, eol)
    }
}

fn read_input<'a>() -> Result<'a, String> {
    parse_input!(parser::input)
}

fn reduced_count<I>(polymer: I) -> usize
where
    I: Iterator<Item = char>,
{
    let mut result = Vec::new();

    for c in polymer {
        match result.last() {
            Some(&c2) if c != c2 && c.to_ascii_uppercase() == c2.to_ascii_uppercase() => {
                result.pop();
            }
            _ => {
                result.push(c);
            }
        }
    }

    result.len()
}

fn stripped_reduced_count(polymer: &str) -> usize {
    "abcdefghijklmnopqrstuvwxyz"
        .chars()
        .map(|c| reduced_count(polymer.chars().filter(|c2| c2.to_ascii_lowercase() != c)))
        .min()
        .unwrap_or(0)
}

fn main() {
    let raw = read_input().unwrap();
    println!("{}", reduced_count(raw.chars()));
    println!("{}", stripped_reduced_count(&raw));
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref RAW: String = read_input().unwrap();
    }

    #[test]
    fn test_reduced_count() {
        assert_eq!(11540, reduced_count(RAW.chars()));
    }

    #[test]
    fn test_stripped_reduced_count() {
        assert_eq!(6918, stripped_reduced_count(&*RAW));
    }
}
