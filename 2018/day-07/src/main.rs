use commons::{lines, parse_input, Result};
use maplit::{btreemap, btreeset, hashmap, hashset};
use std::collections::{BTreeMap, BTreeSet, HashMap, HashSet};
use std::mem;

#[derive(Debug, PartialEq, Eq)]
struct StepOrdering {
    before: char,
    after: char,
}

impl StepOrdering {
    fn new(before: char, after: char) -> Self {
        StepOrdering { before, after }
    }
}

mod parser {
    use super::StepOrdering;
    use nom::types::CompleteStr;
    use nom::{do_parse, map, named, tag, take, verify, IResult};

    named!(letter<CompleteStr, char>,
           verify!(map!(take!(1), |c| c.chars().next().unwrap()), |c| c >= 'A' && c <= 'Z')
    );

    #[rustfmt::skip]
    pub(super) fn line(i: CompleteStr) -> IResult<CompleteStr, StepOrdering> {
           do_parse!(i,
                       tag!("Step ")                          >>
               before: letter                                 >>
                       tag!(" must be finished before step ") >>
               after:  letter                                 >>
                       tag!(" can begin.")                    >>
               (StepOrdering::new(before, after))
           )
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_parse_line() {
            assert_eq!(
                StepOrdering::new('A', 'B'),
                line(CompleteStr(
                    &"Step A must be finished before step B can begin.".to_string()
                ))
                .unwrap()
                .1
            );
        }
    }
}

fn read_input<'a>() -> Result<'a, Vec<StepOrdering>> {
    parse_input!(lines!(parser::line))
}

struct Assembly {
    completed: String,
    ready: BTreeSet<char>,
    remaining: HashMap<char, HashSet<char>>,
}

impl Assembly {
    fn new(orderings: &[StepOrdering]) -> Self {
        let mut remaining = hashmap! {};
        for &StepOrdering { before, after } in orderings {
            remaining.entry(before).or_insert_with(|| hashset! {});
            remaining
                .entry(after)
                .or_insert_with(|| hashset! {})
                .insert(before);
        }
        let ready = remaining
            .iter()
            .filter_map(|(&after, before)| if before.is_empty() { Some(after) } else { None })
            .collect();
        remaining.retain(|_, v| !v.is_empty());
        Assembly {
            completed: String::new(),
            ready,
            remaining,
        }
    }

    fn complete(&mut self, step: char) {
        self.completed.push(step);
        self.ready.remove(&step);
        for (before, after) in self.remaining.iter_mut() {
            after.remove(&step);
            if after.is_empty() {
                self.ready.insert(*before);
            }
        }
        self.remaining.retain(|_, v| !v.is_empty());
    }

    fn split_off_work_items(&mut self, n: usize) -> BTreeSet<char> {
        let mut new_ready;
        let next = self.ready.iter().skip(n).cloned().next();
        match next {
            None => {
                new_ready = btreeset! {};
            }
            Some(head) => {
                new_ready = self.ready.split_off(&head);
            }
        }
        mem::swap(&mut new_ready, &mut self.ready);
        new_ready
    }

    fn top_off(
        &mut self,
        in_progress: &mut BTreeMap<u32, Vec<char>>,
        worker_count: usize,
        work_time_offset: u32,
    ) {
        for s in self.split_off_work_items(worker_count - in_progress.len()) {
            let e = in_progress.entry(work_time(s, work_time_offset)).or_default();
            e.push(s);
        }
    }
}

fn assembly_sequence(orderings: &[StepOrdering]) -> String {
    let mut a = Assembly::new(orderings);
    while !a.ready.is_empty() {
        let s = *a.ready.iter().next().unwrap();
        a.complete(s);
    }
    a.completed
}

fn work_time(step: char, time_offset: u32) -> u32 {
    (step as u32) - ('A' as u32) + time_offset + 1
}

fn parallel_assembly_time(
    worker_count: usize,
    work_time_offset: u32,
    orderings: &[StepOrdering],
) -> u32 {
    let mut a = Assembly::new(orderings);
    let mut in_progress = btreemap! {};
    a.top_off(&mut in_progress, worker_count, work_time_offset);
    let mut total_time = 0;
    while !in_progress.is_empty() {
        let (time, completed) = in_progress.iter().next().unwrap();
        total_time = *time;
        for &s in completed {
            a.complete(s);
        }
        in_progress.remove(&total_time);
        a.top_off(&mut in_progress, worker_count, work_time_offset + total_time);
    }
    total_time
}

fn main() {
    let steps = read_input().unwrap();
    println!("Assembly sequence: {}", assembly_sequence(&steps));
    println!(
        "Parallel assembly time: {}",
        parallel_assembly_time(5, 60, &steps)
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_assembly() {
        let a = Assembly::new(&[StepOrdering::new('A', 'B')]);
        assert_eq!(hashmap! {'B' => hashset!{'A'}}, a.remaining);
        assert_eq!(btreeset! {'A'}, a.ready);
    }

    #[test]
    fn test_complete_assembly_step() {
        let mut a = Assembly::new(&[StepOrdering::new('A', 'B'), StepOrdering::new('B', 'C')]);
        a.complete('A');
        assert_eq!("A", a.completed);
        assert_eq!(btreeset! {'B'}, a.ready);
        assert_eq!(hashmap! {'C' => hashset!{'B'}}, a.remaining);
    }

    #[test]
    fn test_assembly_sequence() {
        assert_eq!(
            "CABDFE",
            assembly_sequence(&[
                StepOrdering::new('C', 'A'),
                StepOrdering::new('C', 'F'),
                StepOrdering::new('A', 'B'),
                StepOrdering::new('A', 'D'),
                StepOrdering::new('B', 'E'),
                StepOrdering::new('D', 'E'),
                StepOrdering::new('F', 'E'),
            ])
        );
    }

    #[test]
    fn test_split_off_work_items_with_enough_items() {
        let mut a = Assembly::new(&[
            StepOrdering::new('A', 'Z'),
            StepOrdering::new('B', 'Z'),
            StepOrdering::new('C', 'Z'),
        ]);
        let items = a.split_off_work_items(2);
        assert_eq!(btreeset! {'A', 'B'}, items);
        assert_eq!(btreeset! {'C'}, a.ready);
    }

    #[test]
    fn test_split_off_work_items_with_too_few_items() {
        let mut a = Assembly::new(&[StepOrdering::new('A', 'Z')]);
        let items = a.split_off_work_items(2);
        assert_eq!(btreeset! {'A'}, items);
        assert_eq!(btreeset! {}, a.ready);
    }

    #[test]
    fn test_parallel_assembly_time() {
        assert_eq!(
            15,
            parallel_assembly_time(
                2,
                0,
                &[
                    StepOrdering::new('C', 'A'),
                    StepOrdering::new('C', 'F'),
                    StepOrdering::new('A', 'B'),
                    StepOrdering::new('A', 'D'),
                    StepOrdering::new('B', 'E'),
                    StepOrdering::new('D', 'E'),
                    StepOrdering::new('F', 'E'),
                ]
            )
        );
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref STEPS: Vec<StepOrdering> = read_input().unwrap();
    }

    #[test]
    fn test_find_assembly_sequence() {
        assert_eq!("ACBDESULXKYZIMNTFGWJVPOHRQ", assembly_sequence(&*STEPS));
    }

    #[test]
    fn test_parallel_assembly_time() {
        assert_eq!(980, parallel_assembly_time(5, 60, &*STEPS));
    }
}
