use std::collections::HashSet;
fn main() {
    println!("Minimal value for R[0] = {}", part_1());
    println!("Maximal value for R[0] = {}", part_2());
}

fn part_1() -> i64 {
    run(Some)
}

fn part_2() -> i64 {
    let mut previous = 0;
    let mut found = HashSet::new();
    run(|r| {
        if found.insert(r) {
            previous = r;
            None
        } else {
            Some(previous)
        }
    })
}

fn run<F>(mut f: F) -> i64
where
    F: FnMut(i64) -> Option<i64>,
{
    let mut a = 0;
    loop {
        let mut b = a | 0x1_0000;
        a = 1_397_714;
        while b > 0 {
            a = (((a + (b & 0xff)) & 0xff_ffff) * 65_899) & 0xff_ffff;
            b /= 256;
        }
        if let Some(r) = f(a) {
            return r;
        }
    }
}
