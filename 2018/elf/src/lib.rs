use std::fmt;

pub use crate::uninstantiated::instructions::*;

pub mod uninstantiated {
    use super::*;

    #[derive(Clone, Copy, Eq, Hash, PartialEq)]
    pub enum Operation {
        Add,
        Mul,
        Ban,
        Bor,
        Set,
        Gt,
        Eq,
    }

    impl fmt::Display for Operation {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            use Operation::*;
            write!(
                f,
                "{}",
                match self {
                    Add => "+",
                    Mul => "*",
                    Ban => "&",
                    Bor => "|",
                    Set => "",
                    Gt => ">",
                    Eq => "==",
                }
            )
        }
    }

    impl Operation {
        pub fn run<A, B>(self, a: A, b: B) -> usize
        where
            A: FnOnce() -> usize,
            B: FnOnce() -> usize,
        {
            use Operation::*;
            match self {
                Add => a() + b(),
                Mul => a() * b(),
                Ban => a() & b(),
                Bor => a() | b(),
                Set => a(),
                Gt => {
                    if a() > b() {
                        1
                    } else {
                        0
                    }
                }
                Eq => {
                    if a() == b() {
                        1
                    } else {
                        0
                    }
                }
            }
        }
    }

    #[derive(Clone, Copy, Eq, Hash, PartialEq)]
    enum AddressMode {
        None,
        Immediate,
        Register,
    }

    impl AddressMode {
        fn instantiate(self, v: usize) -> instantiated::Operand {
            match self {
                AddressMode::None => instantiated::Operand::None,
                AddressMode::Immediate => instantiated::Operand::Immediate(v),
                AddressMode::Register => instantiated::Operand::Register(v),
            }
        }
    }

    #[derive(Clone, Copy, Eq, Hash, PartialEq)]
    pub struct Instruction {
        op: Operation,
        a: AddressMode,
        b: AddressMode,
    }

    impl Instruction {
        pub fn instantiate(self, a: usize, b: usize, c: usize) -> instantiated::Instruction {
            instantiated::Instruction::new(self.op, self.a.instantiate(a), self.b.instantiate(b), c)
        }

        pub fn run(self, regs: &mut instantiated::Registers, a: usize, b: usize, c: usize) {
            self.instantiate(a, b, c).run(regs)
        }
    }

    pub mod instructions {
        use super::AddressMode::*;
        use super::Instruction;
        use super::Operation::*;

        pub static ADDR: Instruction = Instruction {
            op: Add,
            a: Register,
            b: Register,
        };

        pub static ADDI: Instruction = Instruction {
            op: Add,
            a: Register,
            b: Immediate,
        };

        pub static MULR: Instruction = Instruction {
            op: Mul,
            a: Register,
            b: Register,
        };

        pub static MULI: Instruction = Instruction {
            op: Mul,
            a: Register,
            b: Immediate,
        };

        pub static BANR: Instruction = Instruction {
            op: Ban,
            a: Register,
            b: Register,
        };

        pub static BANI: Instruction = Instruction {
            op: Ban,
            a: Register,
            b: Immediate,
        };

        pub static BORR: Instruction = Instruction {
            op: Bor,
            a: Register,
            b: Register,
        };

        pub static BORI: Instruction = Instruction {
            op: Bor,
            a: Register,
            b: Immediate,
        };

        pub static SETR: Instruction = Instruction {
            op: Set,
            a: Register,
            b: None,
        };

        pub static SETI: Instruction = Instruction {
            op: Set,
            a: Immediate,
            b: None,
        };

        pub static GTIR: Instruction = Instruction {
            op: Gt,
            a: Immediate,
            b: Register,
        };

        pub static GTRI: Instruction = Instruction {
            op: Gt,
            a: Register,
            b: Immediate,
        };

        pub static GTRR: Instruction = Instruction {
            op: Gt,
            a: Register,
            b: Register,
        };

        pub static EQIR: Instruction = Instruction {
            op: Eq,
            a: Immediate,
            b: Register,
        };

        pub static EQRI: Instruction = Instruction {
            op: Eq,
            a: Register,
            b: Immediate,
        };

        pub static EQRR: Instruction = Instruction {
            op: Eq,
            a: Register,
            b: Register,
        };

        pub static INSTRUCTIONS: [Instruction; 16] = [
            ADDR, ADDI, MULR, MULI, BANR, BANI, BORR, BORI, SETR, SETI, GTIR, GTRI, GTRR, EQIR, EQRI,
            EQRR,
        ];
    }
}

pub mod instantiated {
    use super::*;

    pub type Registers = [usize];

    #[derive(Clone, Copy)]
    pub enum Operand {
        None,
        Immediate(usize),
        Register(usize),
    }

    impl fmt::Display for Operand {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            use Operand::*;
            match self {
                None => panic!(),
                Immediate(w) => write!(f, "{}", w),
                Register(r) => write!(f, "r[{}]", r),
            }
        }
    }

    impl Operand {
        fn get(self, regs: &Registers) -> usize {
            use Operand::*;
            match self {
                None => panic!(),
                Immediate(w) => w,
                Register(r) => regs[r],
            }
        }
    }

    #[derive(Clone, Copy)]
    struct Computation {
        op: uninstantiated::Operation,
        a: Operand,
        b: Operand,
    }

    impl fmt::Display for Computation {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            use uninstantiated::Operation::*;
            match self.op {
                Set => write!(f, "{}", self.a),
                Gt | Eq => write!(f, "if {} {} {} {{ 1 }} else {{ 0 }}", self.a, self.op, self.b),
                _ => write!(f, "{} {} {}", self.a, self.op, self.b),
            }
        }
    }

    impl Computation {
        fn run(self, regs: &Registers) -> usize {
            self.op.run(|| self.a.get(regs), || self.b.get(regs))
        }
    }

    #[derive(Clone, Copy)]
    pub struct Instruction {
        c: usize,
        computation: Computation,
    }

    impl fmt::Display for Instruction {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            use Operand::Register;

            write!(f, "{} = {}", Register(self.c), self.computation)
        }
    }

    impl Instruction {
        pub fn new(op: uninstantiated::Operation, a: Operand, b: Operand, c: usize) -> Self {
            Instruction {
                c,
                computation: Computation { op, a, b },
            }
        }

        pub fn run(self, regs: &mut Registers) {
            regs[self.c] = self.computation.run(regs);
        }
    }

    pub struct ElfCode {
        instructions: Vec<Instruction>,
        ip: Option<usize>,
        registers: usize,
    }

    impl ElfCode {
        pub fn without_ip(instructions: Vec<Instruction>) -> Self {
            ElfCode {
                instructions,
                ip: None,
                registers: 4,
            }
        }

        pub fn with_ip(ip: usize, instructions: Vec<Instruction>) -> Self {
            ElfCode {
                instructions,
                ip: Some(ip),
                registers: 6,
            }
        }

        fn write_without_ip(&self, f: &mut fmt::Formatter) -> fmt::Result {
            for inst in self.instructions.iter() {
                writeln!(f, "    {};", inst)?;
            }
            Ok(())
        }

        fn write_with_ip(&self, f: &mut fmt::Formatter) -> fmt::Result {
            writeln!(f, "    match r[{}] {{", self.ip.unwrap())?;
            for (i, instr) in self.instructions.iter().enumerate() {
                writeln!(f, "        {} => {},", i, instr)?;
            }
            writeln!(f, "    }}")
        }
    }

    impl fmt::Display for ElfCode {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            writeln!(f, "fn main() {{")?;
            writeln!(f, "    let mut r = [0; {}];", self.registers)?;
            writeln!(f, "")?;
            if self.ip.is_some() {
                self.write_with_ip(f)?;
            } else {
                self.write_without_ip(f)?;
            }
            writeln!(f, "}}")
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fmt::Write;

    macro_rules! assert_opcode {
        (before: $before: expr, $op: ident, in_a: $i1: expr, in_b: $i2: expr, out_c: $o: expr, after: $after: expr) => {
            let mut reg = $before;
            $op.run(&mut reg, $i1, $i2, $o);
            assert_eq!($after, reg);
        };
    }

    #[test]
    fn test_run_addr() {
        assert_opcode!(
            before: [1, 2, 4, 8],
            ADDR, in_a: 1, in_b: 2, out_c: 3,
            after: [1, 2, 4, 6]
        );
    }

    #[test]
    fn test_format_addr() {
        let mut s = String::new();
        write!(&mut s, "{}", ADDR.instantiate(1, 2, 3)).unwrap();
        assert_eq!("r[3] = r[1] + r[2]", s);
    }

    #[test]
    fn test_run_addi() {
        assert_opcode!(
            before: [1, 2, 4, 8],
            ADDI, in_a: 3, in_b: 7, out_c: 0,
            after: [15, 2, 4, 8]
        );
    }

    #[test]
    fn test_format_addi() {
        let mut s = String::new();
        write!(&mut s, "{}", ADDI.instantiate(3, 7, 0)).unwrap();
        assert_eq!("r[0] = r[3] + 7", s);
    }

    #[test]
    fn test_run_mulr() {
        assert_opcode!(
            before: [3, 5, 0, 0],
            MULR, in_a: 0, in_b: 1, out_c: 2,
            after: [3, 5, 15, 0]
        );
    }

    #[test]
    fn test_format_mulr() {
        let mut s = String::new();
        write!(&mut s, "{}", MULR.instantiate(0, 1, 2)).unwrap();
        assert_eq!("r[2] = r[0] * r[1]", s);
    }

    #[test]
    fn test_run_muli() {
        assert_opcode!(
            before: [3, 5, 0, 0],
            MULI, in_a: 0, in_b: 2, out_c: 1,
            after: [3, 6, 0, 0]
        );
    }

    #[test]
    fn test_format_muli() {
        let mut s = String::new();
        write!(&mut s, "{}", MULI.instantiate(0, 2, 1)).unwrap();
        assert_eq!("r[1] = r[0] * 2", s);
    }

    #[test]
    fn test_run_banr() {
        assert_opcode!(
            before: [0xf8, 0x1f, 0, 0],
            BANR, in_a: 0, in_b: 1, out_c: 1,
            after: [0xf8, 0x18, 0, 0]
        );
    }

    #[test]
    fn test_format_banr() {
        let mut s = String::new();
        write!(&mut s, "{}", BANR.instantiate(0, 1, 1)).unwrap();
        assert_eq!("r[1] = r[0] & r[1]", s);
    }

    #[test]
    fn test_run_bani() {
        assert_opcode!(
            before: [0xf8, 0x1f, 0xa5, 0x5a],
            BANI, in_a: 2, in_b: 0x11, out_c: 1,
            after: [0xf8, 1, 0xa5, 0x5a]
        );
    }

    #[test]
    fn test_format_bani() {
        let mut s = String::new();
        write!(&mut s, "{}", BANI.instantiate(2, 0x11, 1)).unwrap();
        assert_eq!("r[1] = r[2] & 17", s);
    }

    #[test]
    fn test_run_borr() {
        assert_opcode!(
            before: [0xf0, 0x0f, 0, 0],
            BORR, in_a: 0, in_b: 1, out_c: 3,
            after: [0xf0, 0x0f, 0, 0xff]
        );
    }

    #[test]
    fn test_format_borr() {
        let mut s = String::new();
        write!(&mut s, "{}", BORR.instantiate(0, 1, 3)).unwrap();
        assert_eq!("r[3] = r[0] | r[1]", s);
    }

    #[test]
    fn test_run_bori() {
        assert_opcode!(
            before: [0, 0, 0, 0xa5],
            BORI, in_a: 3, in_b: 0x5a, out_c: 0,
            after: [0xff, 0, 0, 0xa5]
        );
    }

    #[test]
    fn test_format_bori() {
        let mut s = String::new();
        write!(&mut s, "{}", BORI.instantiate(3, 0x5a, 0)).unwrap();
        assert_eq!("r[0] = r[3] | 90", s);
    }

    #[test]
    fn test_run_setr() {
        assert_opcode!(
            before: [0, 0, 0, 0xa5],
            SETR, in_a: 3, in_b: 0x5a, out_c: 0,
            after: [0xa5, 0, 0, 0xa5]
        );
    }

    #[test]
    fn test_format_setr() {
        let mut s = String::new();
        write!(&mut s, "{}", SETR.instantiate(3, 0x5a, 0)).unwrap();
        assert_eq!("r[0] = r[3]", s);
    }

    #[test]
    fn test_run_seti() {
        assert_opcode!(
            before: [0, 0, 0, 0],
            SETI, in_a: 3, in_b: 0x5a, out_c: 0,
            after: [3, 0, 0, 0]
        );
    }

    #[test]
    fn test_format_seti() {
        let mut s = String::new();
        write!(&mut s, "{}", SETI.instantiate(3, 0x5a, 0)).unwrap();
        assert_eq!("r[0] = 3", s);
    }

    #[test]
    fn test_run_gtir() {
        assert_opcode!(
            before: [0, 4, 8, 10],
            GTIR, in_a: 3, in_b: 0, out_c: 0,
            after: [1, 4, 8, 10]
        );
    }

    #[test]
    fn test_format_gtir() {
        let mut s = String::new();
        write!(&mut s, "{}", GTIR.instantiate(3, 0, 0)).unwrap();
        assert_eq!("r[0] = if 3 > r[0] { 1 } else { 0 }", s);
    }

    #[test]
    fn test_run_gtri() {
        assert_opcode!(
            before: [48, 24, 12, 6],
            GTRI, in_a: 3, in_b: 10, out_c: 0,
            after: [0, 24, 12, 6]
        );
    }

    #[test]
    fn test_format_gtri() {
        let mut s = String::new();
        write!(&mut s, "{}", GTRI.instantiate(3, 10, 0)).unwrap();
        assert_eq!("r[0] = if r[3] > 10 { 1 } else { 0 }", s);
    }

    #[test]
    fn test_run_gtrr() {
        assert_opcode!(
            before: [48, 24, 12, 6],
            GTRR, in_a: 2, in_b: 3, out_c: 1,
            after: [48, 1, 12, 6]
        );
    }

    #[test]
    fn test_format_gtrr() {
        let mut s = String::new();
        write!(&mut s, "{}", GTRR.instantiate(2, 3, 1)).unwrap();
        assert_eq!("r[1] = if r[2] > r[3] { 1 } else { 0 }", s);
    }

    #[test]
    fn test_run_eqir() {
        assert_opcode!(
            before: [0, 4, 8, 10],
            EQIR, in_a: 8, in_b: 2, out_c: 0,
            after: [1, 4, 8, 10]
        );
    }

    #[test]
    fn test_format_eqir() {
        let mut s = String::new();
        write!(&mut s, "{}", EQIR.instantiate(8, 2, 0)).unwrap();
        assert_eq!("r[0] = if 8 == r[2] { 1 } else { 0 }", s);
    }

    #[test]
    fn test_run_eqri() {
        assert_opcode!(
            before: [48, 24, 10, 6],
            EQRI, in_a: 2, in_b: 10, out_c: 0,
            after: [1, 24, 10, 6]
        );
    }

    #[test]
    fn test_format_eqri() {
        let mut s = String::new();
        write!(&mut s, "{}", EQRI.instantiate(2, 10, 0)).unwrap();
        assert_eq!("r[0] = if r[2] == 10 { 1 } else { 0 }", s);
    }

    #[test]
    fn test_run_eqrr() {
        assert_opcode!(
            before: [48, 24, 6, 6],
            EQRR, in_a: 2, in_b: 3, out_c: 1,
            after: [48, 1, 6, 6]
        );
    }

    #[test]
    fn test_format_eqrr() {
        let mut s = String::new();
        write!(&mut s, "{}", EQRR.instantiate(2, 3, 1)).unwrap();
        assert_eq!("r[1] = if r[2] == r[3] { 1 } else { 0 }", s);
    }
}
