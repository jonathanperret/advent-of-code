use commons::Error::InvalidInput;
use commons::{parse_input, OrInvalid, Result};
use maplit::hashmap;
use std::collections::HashMap;
#[cfg(test)]
use std::iter::once;
use std::ops::{Index, IndexMut};
use Acre::*;

fn main() {
    let area = parse_input!(parser::area).unwrap();
    println!(
        "Resource value after 10 minutes: {}",
        area.clone().evolutions().nth(10).unwrap().resource_value()
    );
    let mut smart_area = cycle_detector::CycleDetector::new(area.clone().evolutions());
    smart_area.fast_skip(1_000_000_000);
    println!(
        "Resource value after 1 000 000 000 minutes: {}",
        smart_area.next().unwrap().resource_value()
    );
}

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
enum Acre {
    Open,
    Trees,
    LumberYard,
}

impl Acre {
    #[cfg(test)]
    fn to_char(self) -> char {
        match self {
            Open => '.',
            Trees => '|',
            LumberYard => '#',
        }
    }
}

#[derive(Clone, Eq, PartialEq, Hash)]
struct Area {
    width: usize,
    field: Vec<Acre>,
}

impl Index<[usize; 2]> for Area {
    type Output = Acre;

    fn index(&self, [x, y]: [usize; 2]) -> &Acre {
        &self.field[x + y * self.width]
    }
}

impl IndexMut<[usize; 2]> for Area {
    fn index_mut(&mut self, [x, y]: [usize; 2]) -> &mut Acre {
        &mut self.field[x + y * self.width]
    }
}

impl Area {
    fn parse<'a, T>(acres: T) -> Result<'a, Area>
    where
        T: IntoIterator,
        T::Item: IntoIterator<Item = Acre>,
    {
        let mut width = None;
        let mut field = vec![];

        for line in acres {
            let old_count = field.len();
            field.extend(line);
            let w = field.len() - old_count;
            if let Some(width) = width {
                if width != w {
                    return Err(InvalidInput);
                }
            } else {
                width = Some(w);
            }
        }

        let width = width.or_invalid()?;
        Ok(Area { width, field })
    }

    #[cfg(test)]
    fn dump(&self) -> String {
        self.field
            .chunks(self.width)
            .flat_map(|c| c.iter().map(|a| a.to_char()).chain(once('\n')))
            .collect()
    }

    fn height(&self) -> usize {
        self.field.len() / self.width
    }

    fn summarize_surroundings(&self, x: usize, y: usize) -> HashMap<Acre, usize> {
        let min_x = x - if x > 0 { 1 } else { 0 };
        let min_y = y - if y > 0 { 1 } else { 0 };
        let max_x = x + if x < self.width - 1 { 1 } else { 0 };
        let max_y = y + if y < self.height() - 1 { 1 } else { 0 };
        let mut res = hashmap! { Trees => 0, LumberYard => 0};

        let (x0, y0) = (x, y);

        for x in min_x..=max_x {
            for y in min_y..=max_y {
                if x == x0 && y == y0 {
                    continue;
                }
                res.entry(self[[x, y]]).and_modify(|e| *e += 1);
            }
        }

        res
    }

    fn evolutions(self) -> AreaEvolutions {
        AreaEvolutions(self)
    }

    fn resource_value(&self) -> usize {
        let wood = self.field.iter().filter(|&&a| a == Trees).count();
        let lumber_yards = self.field.iter().filter(|&&a| a == LumberYard).count();
        wood * lumber_yards
    }
}

struct AreaEvolutions(Area);

impl Iterator for AreaEvolutions {
    type Item = Area;

    fn next(&mut self) -> Option<Area> {
        let AreaEvolutions(area) = self;
        let (mut x, mut y) = (0, 0);
        let mut field = Vec::with_capacity(area.field.len());

        for a in area.field.iter() {
            let surrounding = area.summarize_surroundings(x, y);
            x += 1;
            if x == area.width {
                x = 0;
                y += 1;
            }

            field.push(match a {
                Open if surrounding[&Trees] >= 3 => Trees,
                Trees if surrounding[&LumberYard] >= 3 => LumberYard,
                LumberYard if surrounding[&LumberYard] == 0 || surrounding[&Trees] == 0 => Open,
                &a => a,
            });
        }
        std::mem::swap(&mut area.field, &mut field);

        Some(Area {
            width: area.width,
            field,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;
    use lazy_static::lazy_static;
    use nom::types::CompleteStr;

    #[test]
    fn test_summarize_surroundings() {
        let (_, area) = parser::area(CompleteStr(indoc!(
            "
            .|##
            #..|
            #|#.
            "
        )))
        .unwrap();

        assert_eq!(
            hashmap! { Trees => 2, LumberYard => 4 },
            area.summarize_surroundings(1, 1)
        );

        assert_eq!(
            hashmap! { Trees => 1, LumberYard => 1 },
            area.summarize_surroundings(0, 0)
        );

        assert_eq!(
            hashmap! { Trees => 1, LumberYard => 1 },
            area.summarize_surroundings(3, 2)
        );
    }

    lazy_static! {
        static ref SAMPLE_AREA: Area = {
            let (_, area) = parser::area(CompleteStr(indoc!(
                "
                .#.#...|#.
                .....#|##|
                .|..|...#.
                ..|#.....#
                #.#|||#|#|
                ...#.||...
                .|....|...
                ||...#|.#|
                |.||||..|.
                ...#.|..|.
                "
            )))
            .unwrap();

            area
        };
    }

    #[test]
    fn test_evolve() {
        let area = SAMPLE_AREA.clone().evolutions().nth(1).unwrap();

        assert_eq!(
            indoc!(
                "
                .......##.
                ......|###
                .|..|...#.
                ..|#||...#
                ..##||.|#|
                ...#||||..
                ||...|||..
                |||||.||.|
                ||||||||||
                ....||..|.
                "
            ),
            area.dump()
        );
    }

    #[test]
    fn test_resource_value() {
        let area = SAMPLE_AREA.clone().evolutions().nth(10).unwrap();

        assert_eq!(
            indoc!(
                "
                .||##.....
                ||###.....
                ||##......
                |##.....##
                |##.....##
                |##....##|
                ||##.####|
                ||#####|||
                ||||#|||||
                ||||||||||
                "
            ),
            area.dump()
        );

        assert_eq!(1147, area.resource_value());
    }
}

mod cycle_detector {
    use maplit::hashmap;
    use std::collections::{BTreeMap, HashMap};
    use std::hash::Hash;

    pub(super) struct CycleDetector<I>
    where
        I: Iterator,
        I::Item: Hash + Eq + Clone,
    {
        it: I,
        trace: Vec<I::Item>,
        rev_trace: HashMap<I::Item, usize>,
        cycle: Option<usize>,
    }

    impl<I> CycleDetector<I>
    where
        I: Iterator,
        I::Item: Hash + Eq + Clone,
    {
        pub(super) fn new(it: I) -> Self {
            CycleDetector {
                it,
                trace: vec![],
                rev_trace: hashmap! {},
                cycle: None,
            }
        }

        pub(super) fn fast_skip(&mut self, mut n: usize) {
            while n > 0 && self.cycle.is_none() {
                self.next();
                n -= 1;
            }
            if let Some(c) = &mut self.cycle {
                *c = (*c + n) % self.trace.len();
            }
        }
    }

    impl<I> Iterator for CycleDetector<I>
    where
        I: Iterator,
        I::Item: Hash + Eq + Clone,
    {
        type Item = I::Item;

        fn next(&mut self) -> Option<Self::Item> {
            if let Some(c) = &mut self.cycle {
                *c = (*c + 1) % self.trace.len();
                Some(self.trace[*c].clone())
            } else {
                let res = self.it.next();
                if let Some(t) = &res {
                    if let Some(&offset) = self.rev_trace.get(&t) {
                        self.trace = self
                            .rev_trace
                            .drain()
                            .filter(|(_, v)| *v >= offset)
                            .map(|(k, v)| (v, k))
                            .collect::<BTreeMap<_, _>>()
                            .into_iter()
                            .map(|(_, v)| v)
                            .collect();
                        self.cycle = Some(0);
                    } else {
                        self.rev_trace.insert(t.clone(), self.rev_trace.len());
                    }
                }
                res
            }
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_detect_cycle_parameters() {
            struct Counter {
                i: isize,
            };

            impl Iterator for Counter {
                type Item = isize;

                fn next(&mut self) -> Option<isize> {
                    let Counter { i } = self;
                    *i = (*i + 2) % 7;
                    Some(*i)
                }
            }

            let mut counter = Counter { i: -3 };
            let mut c = CycleDetector::new(&mut counter);

            let n = c.nth(20);
            assert_eq!(4, n.unwrap());

            assert_eq!(vec![1, 3, 5, 0, 2, 4, 6,], c.trace);
            assert_eq!(5, c.cycle.unwrap());

            // underlying counter doesn't move anymore
            assert_eq!(1, counter.i);
        }
    }
}

mod parser {
    use super::Acre::*;
    use super::{Acre, Area};
    use nom::types::CompleteStr;
    use nom::{
        alt, call, char, do_parse, eol, many1, many_m_n, map_res, named, named_args, terminated, value,
        IResult,
    };
    use std::iter::once;

    named!(acre<CompleteStr, Acre>, alt!(
        value!(Open, char!('.')) |
        value!(Trees, char!('|')) |
        value!(LumberYard, char!('#'))
    ));

    named_args!(field(width: usize)<CompleteStr, Vec<Vec<Acre>>>, many1!(
        terminated!(
            many_m_n!(width, width, acre),
            eol
        )
    ));

    #[rustfmt::skip]
    pub(super) fn area(i: CompleteStr) -> IResult<CompleteStr, Area> {
        map_res!(i,
            do_parse!(
                first_line:  many1!(acre)             >>
                             eol                      >>
                width:       value!(first_line.len()) >>
                other_lines: call!(field, width)      >>
                (once(first_line).chain(other_lines.into_iter()))
            ),
            Area::parse
        )
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use indoc::indoc;

        #[test]
        fn test_parse_acre() {
            assert_eq!(Open, acre(CompleteStr(".")).unwrap().1);
            assert_eq!(Trees, acre(CompleteStr("|")).unwrap().1);
            assert_eq!(LumberYard, acre(CompleteStr("#")).unwrap().1);
        }

        #[test]
        fn test_parse_dum_area_roundtrip() {
            assert_eq!(
                indoc!(
                    "
                    .|##
                    #..|
                    #|#.
                    "
                ),
                area(CompleteStr(indoc!(
                    "
                    .|##
                    #..|
                    #|#.
                    "
                )))
                .unwrap()
                .1
                .dump()
            );
        }
    }
}
